<?php

namespace Tests\Routes;

use Tests\CkTestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SuperAdminTest extends CkTestCase
{
    use DatabaseTransactions;

    public function setUp()
    {
        parent::setUp();

        $this->ck = $this->createUser()
                         ->addGroup(1)
                         ->addBranch(1)
                         ->login();
    }

    public function testNewsFeed()
    {
        $this->ck->visit('/')->see('data-view="newsfeed"');
    }

    public function testCategoryRoutes()
    {
        $emptyCategory = $this->createCategory();
        $category1 = $this->createCategory();
        $category2 = $this->createCategory();

        $category1Article = $this->createArticle(['category_id' => $category1->id]);

        $category2Article1 = $this->createArticle(['category_id' => $category2->id]);
        $category2Article2 = $this->createArticle(['category_id' => $category2->id]);

        $this->ck
            ->visit('categories')
            ->see('data-view="categories-list"');

        $this->ck
            ->visit('category/' . $emptyCategory->id)
            ->see('data-view="category-articles"')
            ->see('Denne kategorien har ingen artikler.');

        $this->ck
            ->visit('category/' . $category1->id)
            ->see('data-view="article"')
            ->see($category1Article->title);

        $this->ck
            ->visit('category/' . $category2->id)
            ->see('data-view="category-articles"')
            ->see($category2Article1->title)
            ->see($category2Article2->title);

        $this->ck
            ->visit('categories/create')
            ->see('data-view="categories-create"');

        $this->ck
            ->visit('categories/' . $category1->id . '/edit')
            ->see('data-view="categories-edit"')
            ->see($category1->title);
    }

    public function testArticleRoutes()
    {
        $category1 = $this->createCategory();
        $category1Article = $this->createArticle(['category_id' => $category1->id]);

        $this->ck
            ->visit('articles')
            ->see('data-view="articles-list"');

        $this->ck
            ->visit('article/' . $category1Article->id)
            ->see('data-view="article"')
            ->see($category1Article->title);

        $this->ck
            ->visit('articles/create')
            ->see('data-view="articles-create"');

        $this->ck
            ->visit('articles/' . $category1Article->id . '/edit')
            ->see('data-view="articles-edit"')
            ->see($category1Article->title);
    }

    public function testPostRoutes()
    {
        $post1 = $this->createPost(['branch_id' => 1]);

        $this->ck
            ->visit('posts')
            ->see('data-view="posts-list"');

        $this->ck
            ->visit('posts/' . $post1->id)
            ->see('data-view="post"')
            ->see($post1->title);

        $this->ck
            ->visit('posts/create')
            ->see('data-view="posts-create"');

        $this->ck
            ->visit('posts/' . $post1->id . '/edit')
            ->see('data-view="posts-edit"')
            ->see($post1->title);
    }

    public function testTagRoutes()
    {
        $tag = $this->createTag();

        $this->ck
            ->visit('tags')
            ->see('data-view="tags-list"');

        $this->ck
            ->visit('tags/create')
            ->see('data-view="tags-create"');

        $this->ck
            ->visit('tags/' . $tag->id . '/edit')
            ->see('data-view="tags-edit"')
            ->see($tag->title);
    }

    public function testGroupRoutes()
    {
        $group = $this->createGroup();

        $this->ck
            ->visit('groups')
            ->see('data-view="groups-list"');

        $this->ck
            ->visit('groups/create')
            ->see('data-view="groups-create"');

        $this->ck
            ->visit('groups/' . $group->id . '/edit')
            ->see('data-view="groups-edit"')
            ->see($group->title);
    }

    public function testBranchRoutes()
    {
        $branch = $this->createBranch();

        $this->ck
            ->visit('branches')
            ->see('data-view="branches-list"');

        $this->ck
            ->visit('branches/create')
            ->see('data-view="branches-create"');

        $this->ck
            ->visit('branches/' . $branch->id . '/edit')
            ->see('data-view="branches-edit"')
            ->see($branch->title);
    }

    public function testUserRoutes()
    {
        $this->ck
            ->visit('users')
            ->see('data-view="users-list"');

        $this->ck
            ->visit('users/create')
            ->see('data-view="users-create"');

        $this->ck
            ->visit('users/' . $this->currentUser->id . '/edit')
            ->see('data-view="users-edit"')
            ->see($this->currentUser->email);

        $this->ck
            ->visit('profile')
            ->see('data-view="users-profile"')
            ->see($this->currentUser->email);
    }

}