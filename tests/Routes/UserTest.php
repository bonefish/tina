<?php

namespace Tests\Routes;

use Tests\CkTestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserTest extends CkTestCase
{
    use DatabaseTransactions;

    public function setUp()
    {
        parent::setUp();

        $this->ck = $this->createUser()
                         ->addGroup(3)
                         ->addBranch(2)
                         ->login();
    }

    public function testNewsFeed()
    {
        $this->ck->visit('/')->see('data-view="newsfeed"');
    }

    public function testCategoryRoutes()
    {
        $emptyCategory = $this->createCategory();
        $category1 = $this->createCategory();
        $category2 = $this->createCategory();

        $category1Article = $this->createArticle(['category_id' => $category1->id]);

        $category2Article1 = $this->createArticle(['category_id' => $category2->id]);
        $category2Article2 = $this->createArticle(['category_id' => $category2->id]);

        $this->ck
            ->visit('categories')
            ->see('data-view="newsfeed"');

        $this->ck
            ->visit('category/' . $emptyCategory->id)
            ->see('data-view="category-articles"')
            ->see('Denne kategorien har ingen artikler.');

        $this->ck
            ->visit('category/' . $category1->id)
            ->see('data-view="article"')
            ->see($category1Article->title);

        $this->ck
            ->visit('category/' . $category2->id)
            ->see('data-view="category-articles"')
            ->see($category2Article1->title)
            ->see($category2Article2->title);

        $this->ck
            ->visit('categories/create')
            ->see('data-view="newsfeed"');

        $this->ck
            ->visit('categories/' . $category1->id . '/edit')
            ->see('data-view="newsfeed"');
    }

    public function testArticleRoutes()
    {
        $category1 = $this->createCategory();
        $category1Article = $this->createArticle(['category_id' => $category1->id]);

        $this->ck
            ->visit('articles')
            ->see('data-view="newsfeed"');

        $this->ck
            ->visit('article/' . $category1Article->id)
            ->see('data-view="article"')
            ->see($category1Article->title);

        $this->ck
            ->visit('articles/create')
            ->see('data-view="newsfeed"');

        $this->ck
            ->visit('articles/' . $category1Article->id . '/edit')
            ->see('data-view="newsfeed"');
    }

    public function testPostRoutes()
    {
        $post1 = $this->createPost(['branch_id' => 1]);
        $post1->save(['created_by' => 1]);

        $this->ck
            ->visit('posts')
            ->see('data-view="newsfeed"');

        $this->ck
            ->visit('posts/' . $post1->id)
            ->see('data-view="post"')
            ->see($post1->title);

        $this->ck
            ->visit('posts/create')
            ->see('data-view="posts-create"');

        // TO DO - CAN EDIT OWN POST

        // TO DO - CAN'T EDIT OTHER POSTS
    }

    public function testTagRoutes()
    {
        $tag = $this->createTag();

        $this->ck
            ->visit('tags')
            ->see('data-view="newsfeed"');

        $this->ck
            ->visit('tags/create')
            ->see('data-view="newsfeed"');

        $this->ck
            ->visit('tags/' . $tag->id . '/edit')
            ->see('data-view="newsfeed"');
    }

    public function testGroupRoutes()
    {
        $group = $this->createGroup();

        $this->ck
            ->visit('groups')
            ->see('data-view="newsfeed"');

        $this->ck
            ->visit('groups/create')
            ->see('data-view="newsfeed"');

        $this->ck
            ->visit('groups/' . $group->id . '/edit')
            ->see('data-view="newsfeed"');
    }

    public function testBranchRoutes()
    {
        $branch = $this->createBranch();

        $this->ck
            ->visit('branches')
            ->see('data-view="newsfeed"');

        $this->ck
            ->visit('branches/create')
            ->see('data-view="newsfeed"');

        $this->ck
            ->visit('branches/' . $branch->id . '/edit')
            ->see('data-view="newsfeed"');
    }

    public function testUserRoutes()
    {
        $this->ck
            ->visit('users')
            ->see('data-view="newsfeed"');

        $this->ck
            ->visit('users/create')
            ->see('data-view="newsfeed"');

        $this->ck
            ->visit('users/' . $this->currentUser->id . '/edit')
            ->see('data-view="newsfeed"');

        $this->ck
            ->visit('profile')
            ->see('data-view="users-profile"')
            ->see($this->currentUser->email);
    }

}