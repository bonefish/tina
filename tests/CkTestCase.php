<?php

namespace Tests;

use Auth;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CkTestCase extends TestCase
{
    use DatabaseTransactions;

    protected $ck;

    protected $currentUser;

    public function createUser($data = [])
    {
        $this->currentUser = factory(\App\User::class)->create($data);

        return $this;
    }

    public function createCategory($data = [])
    {
        return factory(\App\Ck\Models\Category::class)->create($data);
    }

    public function createArticle($data = [])
    {
        return factory(\App\Ck\Models\Article::class)->create($data);
    }

    public function createPost($data = [])
    {
        return factory(\App\Ck\Models\Post::class)->create($data);
    }

    public function createTag($data = [])
    {
        return factory(\App\Ck\Models\Tag::class)->create($data);
    }

    public function createGroup($data = [])
    {
        return factory(\App\Ck\Models\Group::class)->create($data);
    }

    public function createBranch($data = [])
    {
        return factory(\App\Ck\Models\Branch::class)->create($data);
    }

    public function addGroup($group_id)
    {
        $this->currentUser->groups()->attach($group_id);

        return $this;
    }

    public function addGroups($groups)
    {
        $this->currentUser->groups()->sync($groups);

        return $this;
    }

    public function addBranch($group_id)
    {
        $this->currentUser->branches()->attach($group_id);

        return $this;
    }

    public function addBranches($branches = [])
    {
        $this->currentUser->branches()->sync($branches);

        return $this;
    }

    protected function login()
    {
        Auth::loginUsingId($this->currentUser->id);

        return $this;
    }

}
