<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Ck\Models\Branch;
use App\Ck\Models\Group;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(UserTableSeeder::class);
        $this->call(BranchTableSeeder::class);
        $this->call(GroupTableSeeder::class);

        Model::reguard();
    }
}

class UserTableSeeder extends Seeder
{

    public function run()
    {
        DB::table('users')->delete();

        User::create(['name' => 'Deividas Džiovalas', 'email' => 'deividas@bonefish.no', 'password' => \Hash::make('BonefishDeividas'), 'active' => 1]);
        User::create(['name' => 'Fredrik Enevold', 'email' => 'fredrik@bonefish.no', 'password' => \Hash::make('BonefishFredrik'), 'active' => 1]);
        User::create(['name' => 'Kyrre Amundsen', 'email' => 'kyrre@bonefish.no', 'password' => \Hash::make('BonefishKyrre'), 'active' => 1]);
        User::create(['name' => 'Ove Hellevang', 'email' => 'ove@bonefish.no', 'password' => \Hash::make('BonefishOve'), 'active' => 1]);
    }

}

class BranchTableSeeder extends Seeder
{

    public function run()
    {
        DB::table('branches')->delete();

        Branch::create(['title' => 'CK headquarters']);
    }

}

class GroupTableSeeder extends Seeder
{

    public function run()
    {
        DB::table('groups')->delete();

        Group::create(['id' => 1, 'title' => 'Administrators']);
        Group::create(['id' => 2, 'title' => 'Clinic Admin']);
    }
}
