<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name'           => $faker->name,
        'email'          => $faker->email,
        'password'       => bcrypt(str_random(10)),
        'avatar'         => null,
        'active'         => 1,
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Ck\Models\Category::class, function (Faker\Generator $faker) {
    return [
        'title'     => $faker->sentence(3),
        'icon'      => null,
        'parent_id' => 0
    ];
});

$factory->define(App\Ck\Models\Article::class, function (Faker\Generator $faker) {
    return [
        'title'       => $faker->sentence(3),
        'lead'        => $faker->sentence(12),
        'content'     => $faker->text(),
        'image'       => null,
        'category_id' => 0,
        'state'       => 1,
        'created_by'  => 0,
        'updated_by'  => 0,
        'created_at'  => Carbon::createFromTimeStamp($faker->dateTimeBetween('-1 month', '-5 days')->getTimestamp()),
        'updated_at'  => Carbon::createFromTimeStamp($faker->dateTimeBetween('-4 days', '-1 day')->getTimestamp())
    ];
});

$factory->define(App\Ck\Models\Post::class, function (Faker\Generator $faker) {
    return [
        'title'      => $faker->sentence(3),
        'lead'       => $faker->sentence(12),
        'content'    => $faker->text(),
        'image'      => null,
        'branch_id'  => 0,
        'group_id'   => 0,
        'must_read'  => 1,
        'state'      => 1,
        'created_by' => 0,
        'updated_by' => 0,
        'created_at' => Carbon::createFromTimeStamp($faker->dateTimeBetween('-1 month', '-5 days')->getTimestamp()),
        'updated_at' => Carbon::createFromTimeStamp($faker->dateTimeBetween('-4 days', '-1 day')->getTimestamp())
    ];
});

$factory->define(App\Ck\Models\Tag::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->word()
    ];
});

$factory->define(App\Ck\Models\Group::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->word()
    ];
});

$factory->define(App\Ck\Models\Branch::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->word()
    ];
});
