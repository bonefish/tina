<?php namespace App\Ck\Models;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class Post extends Model
{

    protected $table = 'posts';

    protected $fillable = ['title', 'lead', 'content', 'image', 'must_read', 'state'];

    public static function boot()
    {
        parent::boot();

        static::creating(function ($table) {
            $table->updated_by = Auth::user()->id;
            $table->created_by = Auth::user()->id;
        });

        static::updating(function ($table) {
            $table->updated_by = Auth::user()->id;
        });

        static::saving(function ($table) {
            $table->updated_by = Auth::user()->id;
        });

        static::deleting(function ($table) {
            $table->groups()->detach();
            $table->branches()->detach();
            $table->tags()->detach();
            $table->read()->detach();
            $table->star()->detach();
            $table->comments()->delete();
            foreach ($table->files as $file) {
                if (Storage::has('files/' . $file->filename)) {
                    Storage::delete('files/' . $file->filename);
                }
            }
            $table->files()->delete();
        });
    }

    public function scopeReaders()
    {
        return DB::table('users')
            ->select('users.name', 'posts_read.created_at')
            ->join('posts_read', 'posts_read.user_id', '=', 'users.id')
            ->where('posts_read.post_id', '=', $this->id)
            ->orderBy('posts_read.created_at', 'DESC')
            ->get();
    }

    public function author()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    public function editor()
    {
        return $this->belongsTo('App\User', 'updated_by');
    }

    public function tags()
    {
        return $this->belongsToMany('App\Ck\Models\Tag', 'post_tags');
    }

    public function groups()
    {
        return $this->belongsToMany('App\Ck\Models\Group', 'group_posts');
    }

    public function branches()
    {
        return $this->belongsToMany('App\Ck\Models\Branch', 'branch_posts');
    }

    public function comments()
    {
        return $this->hasMany('App\Ck\Models\Comment');
    }

    public function read()
    {
        return $this->belongsToMany('App\User', 'posts_read');
    }

    public function star()
    {
        return $this->belongsToMany('App\User', 'post_stars');
    }

    public function files()
    {
        return $this->hasMany('App\Ck\Models\File');
    }
}