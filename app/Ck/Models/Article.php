<?php namespace App\Ck\Models;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Article extends Model {

    protected $table = 'articles';

    protected $fillable = ['title', 'lead', 'content', 'image', 'category_id', 'must_read', 'state'];

    public static function boot()
    {
        parent::boot();

        static::creating(function ($table) {
            $table->updated_by = Auth::user()->id;
            $table->created_by = Auth::user()->id;
        });

        static::updating(function ($table) {
            $table->updated_by = Auth::user()->id;
        });

        static::saving(function ($table) {
            $table->updated_by = Auth::user()->id;
        });

        static::deleting(function ($table) {
            $table->tags()->detach();
            foreach ($table->files as $file) {
                if (Storage::has('files/'. $file->filename)) {
                    Storage::delete('files/'.$file->filename);
                }
            }
            $table->files()->delete();
        });
    }

    public function category()
    {
        return $this->belongsTo('App\Ck\Models\Category');
    }

    public function files() {
        return $this->hasMany('App\Ck\Models\File');
    }

    public function tags() {
        return $this->belongsToMany('App\Ck\Models\Tag', 'article_tags');
    }

    public function author()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    public function editor()
    {
        return $this->belongsTo('App\User', 'updated_by');
    }

}
//php0xleH3.xlsx