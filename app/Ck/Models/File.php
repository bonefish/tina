<?php namespace App\Ck\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class File extends Model {

    protected $table = 'files';

    public $timestamps = false;

    protected $fillable = ['post_id', 'article_id', 'filename', 'original'];

    public static function boot()
    {
        parent::boot();

        static::deleting(function ($table) {
            if (Storage::has('files/'. $table->filename)) {
                Storage::delete('files/'.$table->filename);
            }
        });
    }

    protected function article() {
        return $this->belongsTo('App\Ck\Models\Article');
    }

    protected function post() {
        return $this->belongsTo('App\Ck\Models\Post');
    }

}