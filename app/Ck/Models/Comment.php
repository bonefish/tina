<?php namespace App\Ck\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model {

    protected $table = 'comments';

    protected $fillable = ['post_id', 'user_id', 'comment'];

    protected function user() {
        return $this->belongsTo('App\User');
    }

    protected function post() {
        return $this->belongsTo('App\Ck\Models\Post');
    }

}