<?php namespace App\Ck\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    protected $table = 'categories';

    protected $fillable = ['title', 'icon', 'parent_id'];

    public static function boot()
    {
        parent::boot();

        static::creating(function ($category) {
            if ($category->parent_id == 0) {
                $category->ordering = DB::table('categories')->max('ordering') + 1;
            }
        });

        static::updating(function ($category) {
            if ($category->parent_id > 0) {
                $category->ordering = 0;
            }
        });

        static::saving(function ($category) {
            if ($category->parent_id > 0) {
                $category->ordering = 0;
            }
        });
    }

    public function articles()
    {
        return $this->hasMany(Article::class);
    }

    public function scopeChildren($query)
    {
        return $query->where('parent_id', '=', $this->id)->orderBy('title', 'ASC')->get();
    }

}