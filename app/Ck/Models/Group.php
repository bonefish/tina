<?php

namespace App\Ck\Models;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $table = 'groups';

    protected $fillable = ['title', 'lead', 'content', 'image', 'state', ];

    public function users()
    {
        return $this->belongsToMany('App\User', 'user_groups');
    }

    public function posts()
    {
        return $this->belongsToMany('App\Ck\Models\Post', 'group_posts');
    }
}
