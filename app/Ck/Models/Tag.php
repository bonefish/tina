<?php namespace App\Ck\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model {

    public $timestamps = false;

    protected $table = 'tags';

    protected $fillable = ['title'];

    public static function boot()
    {
        parent::boot();

        static::deleting(function ($table) {
            $table->articles()->detach();
            $table->posts()->detach();
        });
    }

    public function articles()
    {
        return $this->belongsToMany('App\Ck\Models\Article', 'article_tags');
    }

    public function posts()
    {
        return $this->belongsToMany('App\Ck\Models\Post', 'post_tags');
    }

    public function scopeWithPosts($query)
    {
        $post_ids = DB::table('posts')->
            select('id')->
            where('state', 1)->
            lists('id');

        $unique_ids = DB::table('post_tags')->select(DB::Raw('DISTINCT(tag_id) AS id'))->whereIn('post_id', $post_ids)->lists('id');
        return $query->whereIn('id', $unique_ids);
    }

}