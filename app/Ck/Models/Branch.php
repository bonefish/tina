<?php

namespace App\Ck\Models;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    protected $table = 'branches';

    protected $fillable = ['title'];

    public function users()
    {
        return $this->belongsToMany('App\User', 'user_branches');
    }

    public function posts()
    {
        return $this->belongsToMany('App\Ck\Models\Post', 'branch_posts');
    }

}
