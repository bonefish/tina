<?php

namespace App\Ck\Helpers;

use Auth;
use DB;

class AccessHelper
{

    public static function isAdmin()
    {
        return (DB::table('user_groups')->whereIn('group_id', [1, 2])->where('user_id', Auth::user()->id)->count() > 0);
    }

    public static function isSuperAdmin()
    {
        return (DB::table('user_groups')->where('group_id', 1)->where('user_id', Auth::user()->id)->count() > 0);
    }

    public static function canPost()
    {
        return ((count(self::allUserGroups())) > 0 || self::isAdmin());
    }

    public static function canAccessPost($post)
    {
        return (
            self::isAdmin() ||
            ($post->branches()->where('id', 1)->count() > 0) ||
            self::userHasGroup($post->groups()->lists('id')) ||
            self::userHasBranch($post->branches()->lists('id')));
    }

    public static function canEditPost($item)
    {
        return (self::isSuperAdmin() || ($item->created_by == Auth::user()->id));
    }

    public static function userHasGroup($ids)
    {
        return (DB::table('user_groups')->where('user_id', Auth::user()->id)->whereIn('group_id', $ids)->count() > 0);
    }

    public static function userHasBranch($ids)
    {
        return (DB::table('user_branches')->where('user_id', Auth::user()->id)->whereIn('branch_id', $ids)->count() > 0);
    }

    public static function availablePosts($groups = false, $branches = false)
    {
        if (!$groups) {
            $groups = self::allUserGroups();
        }

        if (!$branches) {
            $branches = self::allUserBranches();
        }

        return DB::table('posts')
            ->select('posts.id')
            ->leftJoin('group_posts', 'posts.id', '=', 'group_posts.post_id')
            ->leftJoin('branch_posts', 'posts.id', '=', 'branch_posts.post_id')
            ->where('posts.state', 1)
            ->where(function ($query) use ($groups, $branches) {
                $query->orWhereIn('group_posts.group_id', $groups)
                    ->orWhereIn('branch_posts.branch_id', $branches)
                    ->orWhere('branch_posts.branch_id', 1);
            })
            ->groupBy('posts.id')
            ->lists('posts.id');
    }

    public static function availableBranches()
    {
        if (!self::isAdmin()) {
            return[];
        }

        $query = DB::table('branches');

        if (!self::isSuperAdmin()) {
            $query->whereIn('id', self::allUserBranches());
        }

        return $query->lists('title', 'id');
    }

    public static function groupsForPosting()
    {
        $query = DB::table('groups')->select(['id', 'title']);

        if (!self::isSuperAdmin()) {
            $query->whereIn('id', self::allUserGroups());
        }

        $query->whereNotIn('id', [1, 2]);

        return $query->lists('title', 'id');
    }

    public static function groupsForViewing()
    {
        $query = DB::table('groups')->select(['id', 'title']);
        $query->whereIn('id', self::allUserGroups());

        $query->whereNotIn('id', [1, 2]);

        return $query->get();
    }

    public static function branchesForViewing()
    {
        return DB::table('branches')->
            select(['id', 'title'])->
            whereIn('id', self::allUserBranches())->
            where('id', '!=', 1)->
            get();
    }

    public static function availableGroups()
    {
        $query = DB::table('groups')->select(['id', 'title']);

        if (!self::isSuperAdmin()) {
            $query->whereIn('id', self::allUserGroups());
        }

        return $query->get();
    }

    public static function allUserGroups()
    {
        return DB::table('user_groups')->
            select('group_id')->
            where('user_id', Auth::user()->id)->
            lists('group_id');
    }

    public static function allUserBranches()
    {
        return DB::table('user_branches')->
            select('branch_id')->
            where('user_id', Auth::user()->id)->
            lists('branch_id');
    }

    public static function filterUserGroups($groups)
    {
        return DB::table('user_groups')->
        select('group_id')->
        where('user_id', Auth::user()->id)->
        whereIn('group_id', $groups)->
        groupBy('group_id')->
        lists('group_id');
    }

    public static function filterPostsAccess($ids)
    {
        return DB::table('posts')->
            select('posts.id')->
            leftJoin('group_posts', 'posts.id', '=', 'group_posts.post_id')->
            leftJoin('branch_posts', 'posts.id', '=', 'branch_posts.post_id')->
            whereIn('posts.id', $ids)->
            where(function ($query) {
                $query->whereIn('group_posts.group_id', self::allUserGroups())->
                    orWhereIn('branch_posts.branch_id', self::allUserBranches());
            })->
            lists('id');
    }

}