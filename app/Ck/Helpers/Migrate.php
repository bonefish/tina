<?php

namespace App\Ck\Helpers;

use App\Ck\Models\Category;
use App\Ck\Models\Post;

class Migrate
{
    /*
    public static function updatePosts()
    {
        $posts = Post::all();

        foreach ($posts as $post) {
            if ($post->branch_id > 0) {
                $post->branches()->sync([$post->branch_id]);
            } elseif ($post->group_id > 0) {
                $post->groups()->sync([$post->group_id]);
            }
        }

        die('Posts update done.');
    }
    */
    public static function updateOrdering()
    {
        $categories = Category::whereParentId(0)->orderBy('title', 'ASC')->get();
        foreach ($categories as $i => $category) {
            $category->ordering = $i + 1;
            $category->save();
        }

        return ('Ordering update done');
    }
}