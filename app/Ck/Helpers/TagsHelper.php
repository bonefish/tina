<?php

namespace App\Ck\Helpers;

use DB;

class TagsHelper
{
    public static function getPostsTags($post_ids)
    {
        return DB::table('tags')
                 ->select('tags.*')
                 ->leftJoin('post_tags', 'tags.id', '=', 'post_tags.tag_id')
                 ->whereIn('post_tags.post_id', $post_ids)
                 ->groupBy('tags.id')
                 ->orderBy('tags.title', 'ASC')
                 ->get();
    }
}