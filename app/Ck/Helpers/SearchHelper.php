<?php namespace App\Ck\Helpers;

use Auth;
use DB;

class SearchHelper {

    public static function prepareQuery($query)
    {
        return preg_split('/[\s]+/', $query);
    }

    public static function searchTags($keywords)
    {
        $query = DB::table('tags')->select('id');

        foreach ($keywords as $keyword) {
            $query->orWhere('title', 'LIKE', '%'.$keyword.'%');
        }

        return $query->lists('id');
    }

    public static function searchPosts($keywords, $tagIds) {
        $query = DB::table('posts')->select('id');

        if (count($tagIds)) {
            $query->leftJoin('post_tags', 'posts.id', '=', 'post_tags.post_id');
            $query->whereIn('post_tags.tag_id', $tagIds);
        }

        $query->orWhere(function($query) use ($keywords){
            foreach ($keywords as $keyword) {
                $query->orWhere('title', 'LIKE', '%'.$keyword.'%')
                    ->orWhere('lead', 'LIKE', '%'.$keyword.'%');
            }
        });

        return $query->lists('id');
    }

    public static function searchArticles($keywords, $tagIds) {
        $query = DB::table('articles')->select('id')->where('articles.state', '=', 1);

        if (count($tagIds)) {
            $query->leftJoin('article_tags', 'articles.id', '=', 'article_tags.article_id');
            $query->whereIn('article_tags.tag_id', $tagIds);
        }

        $query->orWhere(function($query) use ($keywords){
            foreach ($keywords as $keyword) {
                $query->orWhere('title', 'LIKE', '%'.$keyword.'%')
                    ->orWhere('lead', 'LIKE', '%'.$keyword.'%')
                    ->orWhere('content', 'LIKE', '%'.$keyword.'%');
            }
        });

        return $query->lists('id');
    }
}