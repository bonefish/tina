<?php namespace App\Ck\Helpers;

class ImageHelper {

    public static function previewImage($image)
    {
        $class = 'img-responsive';
        $options = ['w' => 360];
        return self::renderImage($image, $options, $class);
    }

    public static function articleList($image)
    {
        $class = 'img-responsive';
        $options = [
            'w'   => 500,
            'h'   => 250,
            'fit' => 'crop'
        ];

        return self::renderImage($image, $options, $class);
    }

    public static function userAvatarSmall($image)
    {
        $class = 'img-circle';
        $options = [
            'h'   => 32,
            'w'   => 32,
            'fit' => 'crop'
        ];

        return self::renderAvatar($image, $options, $class);
    }

    public static function userAvatarMedium($image)
    {
        if (empty($image)) {
            $class = 'default-avatar';
        } else {
            $class = 'img-circle';
        }

        $options = [
            'h'   => 48,
            'w'   => 48,
            'fit' => 'crop'
        ];

        return self::renderAvatar($image, $options, $class);
    }

    public static function profileAvatar($image)
    {
        if (empty($image)) {
            $class = 'default-avatar avatar-huge';
        } else {
            $class = 'img-circle';
        }

        $options = [
            'h'   => 128,
            'w'   => 128,
            'fit' => 'crop'
        ];

        return self::renderAvatar($image, $options, $class);
    }

    public static function responsiveImage($image)
    {
        $class = 'img-responsive';

        $options = [
            'w' => 934
        ];

        return self::renderImage($image, $options, $class);
    }

    protected static function renderImage($image, $options, $class)
    {
        if (empty($image)) {
            return '';
        }

        $src = url(\Glide::load($image)->modify($options));

        return '<img src="' . $src . '" class="' . $class . '" />';
    }

    protected static function renderAvatar($image, $options, $class)
    {
        if (empty($image)) {
            return '<div class="' . $class . '"><i class="icon-user"></i></div>';
        }

        return '<img src="' . url(\Glide::load($image)->modify($options)) . '" class="' . $class . '" />';
    }

    public static function feedImage($image)
    {
        if (empty($image)) {
            return '';
        }

        $src = \Glide::load($image)->modify(['w' => 700, 'h' => 350, 'fit' => 'crop']);

        return '<img src="' . url($src) . '" class="img-responsive" />';
    }

    public static function articleImage($image)
    {
        if (empty($image)) {
            return '';
        }

        $src = \Glide::load($image)->modify(['w' => 520, 'h' => 260, 'fit' => 'crop']);

        return '<img src="' . $src . '" class="img-thumbnail" />';
    }

}