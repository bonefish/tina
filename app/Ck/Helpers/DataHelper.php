<?php namespace App\Ck\Helpers;

use App\Ck\Models\Category;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class DataHelper {

    public static function categories()
    {
        $key = 'categories_' . strtotime(DB::table('categories')->max('updated_at'));

        if (!Cache::has($key)) {
            $categories = [];
            $parents = Category::whereParentId(0)->orderBy('ordering', 'ASC')->get();
            foreach ($parents as $parent) {
                $children = $parent->children();

                $categories[$parent->id]['title'] = $parent->title;
                $categories[$parent->id]['icon'] = $parent->icon;
                $categories[$parent->id]['has_children'] = (count($children) > 0);

                if (count($children)) {
                    $categories[$parent->id]['children'] = [];
                    foreach ($children as $child) {
                        $categories[$parent->id]['children'][$child->id]['title'] = $child->title;
                    }
                }
            }

            Cache::forever($key, $categories);
        }


        return Cache::get($key);
    }

}