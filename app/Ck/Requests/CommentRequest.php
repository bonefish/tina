<?php namespace App\Ck\Requests;

use App\Http\Requests\Request;

class CommentRequest extends Request {

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'comment' => 'required',
            'post_id' => 'required'
        ];
    }
}
