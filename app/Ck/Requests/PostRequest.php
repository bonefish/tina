<?php namespace App\Ck\Requests;

use App\Http\Requests\Request;

class PostRequest extends Request {

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'title'   => 'required',
            'lead'    => 'required'
        ];
    }
}
