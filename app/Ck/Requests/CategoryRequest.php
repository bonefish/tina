<?php namespace App\Ck\Requests;

use App\Http\Requests\Request;

class CategoryRequest extends Request
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'title' => 'required'
        ];
    }

}