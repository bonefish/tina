<?php namespace App\Ck\Requests;

use App\Http\Requests\Request;

class UserRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$this->request->get('id'),
            'branches' => 'required'
        ];

        if ($this->request->get('id') == "") {
            $rules['password'] = 'required|confirmed|min:6';
        } else if ($this->request->get('password') != "") {
            $rules['password'] = 'confirmed|min:6';
        }

        return $rules;
    }
}
