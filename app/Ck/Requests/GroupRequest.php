<?php namespace App\Ck\Requests;

use App\Http\Requests\Request;

class GroupRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'title' => 'required|unique:groups,title,'.$this->request->get('id')
        ];
    }
}
