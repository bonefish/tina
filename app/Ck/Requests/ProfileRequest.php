<?php namespace App\Ck\Requests;

use App\Http\Requests\Request;

class ProfileRequest extends Request {

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'name'   => 'required',
            'avatar' => 'image'
        ];

        if (($this->request->get('current_password') != "") || ($this->request->get('password') != "")) {
            $rules['current_password'] = 'required|min:6';
            $rules['password'] = 'required|confirmed|min:6';
        }

        return $rules;
    }

}