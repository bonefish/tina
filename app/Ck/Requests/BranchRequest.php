<?php namespace App\Ck\Requests;

use App\Http\Requests\Request;

class BranchRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'title' => 'required|unique:branches,title,'.$this->request->get('id')
        ];
    }
}
