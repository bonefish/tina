<?php namespace App\Http\Middleware;

use Closure;
use App\Ck\Helpers\AccessHelper;
use Illuminate\Contracts\Auth\Guard;

class EditorMiddleware
{
    protected $auth;

    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    public function handle($request, Closure $next)
    {
        if (!AccessHelper::isAdmin()) {
            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest('/');
            }
        }

        return $next($request);
    }
}
