<?php namespace App\Http\Requests;

class Form2Request extends Request
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'kurs'  => 'required',
            'phone' => 'required'
        ];

        return $rules;
    }
}
