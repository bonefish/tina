<?php namespace App\Http\Requests;

use App\Ck\Helpers\AccessHelper;

class TagsRequest extends Request {

    public function authorize()
    {
        return AccessHelper::isSuperAdmin();
    }

    public function rules()
    {
        return [
            'title' => 'required'
        ];
    }
}
