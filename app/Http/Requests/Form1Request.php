<?php namespace App\Http\Requests;

class Form1Request extends Request
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'title' => 'required',
            'text'  => 'required'
        ];

        return $rules;
    }
}
