<?php namespace App\Http\Requests;

class ArticleRequest extends Request
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'title'       => 'required',
            'category_id' => 'required'
        ];

        if ($this->request->get('category_id') != 16) {
            $rules['lead'] = 'required';
            $rules['content'] = 'required';
        }

        return $rules;
    }
}
