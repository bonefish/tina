<?php namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;

class AjaxPostRequest extends Request {

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'title' => 'required',
            'lead'  => 'required'
        ];
    }

    protected function formatErrors(Validator $validator)
    {
        return $validator->errors()->all();
    }
}
