<?php namespace App\Http\Controllers;

use Auth;
use Mail;
use App\Http\Requests\Form1Request;
use App\Http\Requests\Form2Request;

class FormsController extends Controller
{

    public function form1(Form1Request $request)
    {
        $data = $request->all();
        Mail::send('emails.form1', $data, function ($msg) {
            $msg->from('no-reply@colosseumatwork.no', 'Colosseum@work');
            $msg->to('tori@colosseum.no');
            $msg->subject('Nytt tips til HQ fra Colosseum@work');
        });
        flash(trans('messages.form_filled'));
        return redirect()->back();
    }

    public function form2(Form2Request $request)
    {
        $data = $request->all();
        Mail::send('emails.form2', $data, function ($msg) {
            $msg->from('no-reply@colosseumatwork.no', 'Colosseum@work');
            $msg->to('siri@colosseum.no');
            $msg->subject('Ny påmelding fra Colosseum@work');
        });
        flash(trans('messages.form_filled2'));
        return redirect()->back();
    }

}