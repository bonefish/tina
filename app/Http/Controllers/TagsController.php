<?php namespace App\Http\Controllers;

use App\Ck\Models\Tag;
use App\Http\Requests\TagsRequest;

class TagsController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        view()->share('active', 'tags');
    }

    public function index()
    {
        view()->share('view', 'tags-list');

        $items = Tag::orderBy('title', 'ASC')->paginate(20);
        return View('tags.index', compact('items'));
    }

    public function create()
    {
        view()->share('view', 'tags-create');

        $item = new Tag();
        return View('tags.form', compact('item'));
    }

    public function store(TagsRequest $request)
    {
        Tag::create($request->all());
        return redirect('tags');
    }

    public function edit($id)
    {
        view()->share('view', 'tags-edit');

        $item = Tag::find($id);
        return View('tags.form', compact('item'));
    }

    public function update(TagsRequest $request, $id)
    {
        $item = Tag::findOrFail($id);
        $item->update($request->all());
        return redirect('tags');
    }

    public function destroy($id)
    {
        Tag::findOrFail($id)->delete();
        return redirect('tags');
    }

}
