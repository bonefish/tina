<?php namespace App\Http\Controllers;

use App\Ck\Requests\BranchRequest;
use App\Ck\Models\Branch;

class BranchesController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        view()->share('active', 'branches');
    }

    public function index()
    {
        view()->share('view', 'branches-list');

        $items = Branch::orderBy('title', 'ASC')->get();
        return View('branches.index', compact('items'));
    }

    public function create()
    {
        view()->share('view', 'branches-create');

        $item = new Branch();
        return View('branches.form', compact('item'));
    }

    public function store(BranchRequest $request)
    {
        Branch::create($request->all());
        return redirect('branches');
    }

    public function edit($id)
    {
        view()->share('view', 'branches-edit');

        $item = Branch::find($id);
        return View('branches.form', compact('item'));
    }

    public function update(BranchRequest $request, $id)
    {
        $item = Branch::find($id);
        $item->update($request->all());
        return redirect('branches');
    }

}
