<?php namespace App\Http\Controllers;

use App\Ck\Models\Article;
use App\Http\Requests\ArticleRequest;
use Illuminate\Support\Facades\Storage;

class ArticlesController extends Controller
{

    public function __construct()
    {
        parent::__construct();
        view()->share('active', 'articles');
    }

    public function index()
    {
        view()->share('view', 'articles-list');

        $items = Article::orderBy('created_at', 'DESC')->paginate(20);

        return view('articles.index', compact('items'));
    }

    public function create()
    {
        view()->share('view', 'articles-create');

        $item = new Article();
        return View('articles.form', compact('item'));
    }

    public function store(ArticleRequest $request)
    {
        $data = $request->all();

        $data['state'] = isset($data['state']) ? 1 : 0;

        $item = Article::create($data);

        if (!empty($request->input('image'))) {
            $image = $request->input('image');
            Storage::move('temp/'.$image, 'images/'.$image);
        }

        foreach ($request->input('files', []) as $file) {
            \App\Ck\Models\File::find($file)->update(['article_id' => $item->id]);
        }

        if (!isset($data['tags'])) {
            $data['tags'] = array();
        }

        $item->tags()->sync($data['tags']);

        return redirect('articles');
    }

    public function edit($id)
    {
        view()->share('view', 'articles-edit');

        $item = Article::find($id);
        return View('articles.form', compact('item'));
    }

    public function update(ArticleRequest $request, $id)
    {
        $item = Article::find($id);
        $data = $request->all();

        $data['state'] = isset($data['state']) ? 1 : 0;

        if ($item->image != $request->input('image')) {
            $new_image = $request->input('image');
            Storage::move('temp/'.$new_image, 'images/'.$new_image);
            if (!empty($item->image) && Storage::has('images/'.$item->image)) {
                Storage::delete('images/'.$item->image);
            }
        }

        $item->update($data);

        foreach ($request->input('files', []) as $file) {
            \App\Ck\Models\File::find($file)->update(['article_id' => $item->id]);
        }

        if (!isset($data['tags'])) {
            $data['tags'] = array();
        }

        $item->tags()->sync($data['tags']);

        return redirect('articles');
    }

    public function show($id)
    {
        view()->share('view', 'article');

        $item = Article::findOrFail($id);

        if (isset($item->category_id)) {
            view()->share('active', 'category'.$item->category_id);
        }

        if ($item->state != 1) {
            return View('articles.notfound');
        }

        return View('articles.show', compact('item'));
    }

    public function read($id)
    {
        $user = \Auth::user();
        $item = Article::find($id);

        if (!$item->read->contains($user->id)) {
            $item->read()->attach($user->id);
        }

        return redirect()->back();
    }

    public function destroy($id)
    {
        Article::find($id)->delete();
        return redirect()->back();
    }

    public function deleteImage($id)
    {
        $item = Article::find($id);

        if (!empty($item->image)) {
            if (Storage::has('images/'.$item->image)) {
                Storage::delete('images/'.$item->image);
            }
            $item->image = null;
            $item->save();
        }

        return redirect('articles/'.$item->id.'/edit');
    }

}
