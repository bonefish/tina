<?php

namespace App\Http\Controllers;

use App\Ck\Helpers\TagsHelper;
use DB;
use Auth;
use App\Ck\Models\Tag;
use App\Ck\Models\Group;
use App\Ck\Models\Branch;
use App\Ck\Helpers\AccessHelper;

class HomeController extends Controller
{

    public function __construct()
    {
        parent::__construct();
        view()->share('active', '/');
        view()->share('view', 'newsfeed');
    }

    public function getFeed($filter = null, $param = null)
    {
        $user_groups = AccessHelper::allUserGroups();
        $filter_groups = AccessHelper::groupsForViewing();
        $filter_branches = AccessHelper::branchesForViewing();

        $available_posts = AccessHelper::availablePosts();

        $tags = TagsHelper::getPostsTags($available_posts);

        $active = [
            'tag'    => null,
            'group'  => null,
            'branch' => null
        ];

        $query = DB::table('posts')
                   ->select([
                       'posts.*',
                       DB::raw('COUNT(post_stars.post_id) AS star'),
                       DB::raw('COUNT(posts_read.post_id) AS isread'),
                       DB::raw('IF(posts.must_read = 1, IF(COUNT(posts_read.post_id) > 0, 0, 1), 0) AS haveread'),
                       DB::raw('COUNT(branch_posts.post_id) AS branches'),
                       DB::raw('COUNT(group_posts.post_id) AS groups'),
                   ])
                   ->leftJoin('post_stars', function ($query) {
                       $query->on('posts.id', '=', 'post_stars.post_id')
                             ->where('post_stars.user_id', '=', Auth::user()->id);
                   })
                   ->leftJoin('posts_read', function ($query) {
                       $query->on('posts.id', '=', 'posts_read.post_id')
                             ->where('posts_read.user_id', '=', Auth::user()->id);
                   })
                   ->leftJoin('branch_posts', 'posts.id', '=', 'branch_posts.post_id')
                   ->leftJoin('group_posts', 'posts.id', '=', 'group_posts.post_id')
                   ->whereIn('posts.id', $available_posts);

        switch ($filter) {
            case 'main':
                $query->where('branch_posts.branch_id', '=', 1);
                break;
            case 'clinic':
                if (!in_array($param, AccessHelper::allUserBranches())) {
                    return redirect('/');
                }
                $active['branch'] = Branch::find($param)->title;
                $query->where('branch_posts.branch_id', '=', $param);
                break;
            case 'groups':
                if (is_null($param)) {
                    $query->whereIn('group_posts.group_id', $user_groups);
                } else {
                    $active['group'] = Group::find($param)->title;
                    $query->where('group_posts.group_id', $param);
                }
                break;
            case 'tags':
                if ($param) {
                    $active['tag'] = Tag::find($param)->title;
                }
                $query->leftJoin('post_tags', 'posts.id', '=', 'post_tags.post_id')
                      ->where('post_tags.tag_id', $param);
                break;
            case 'starred':
                $query->where('post_stars.user_id', '=', Auth::user()->id);
                break;
            default:
                break;
        }

        $query->orderBy('haveread', 'DESC')
              ->orderBy('posts.updated_at', 'DESC')
              ->groupBy('posts.id');

        $items = $query->paginate(20);

        return View(
            'pages.newsfeed',
            compact('items', 'tags', 'active', 'user_groups', 'filter_groups', 'filter_branches')
        );
    }
}