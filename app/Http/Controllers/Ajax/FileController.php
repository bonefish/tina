<?php namespace App\Http\Controllers\Ajax;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class FileController extends Controller {

    public function uploadImage(Request $request)
    {
        if (!$request->hasFile('upload-image')) {
            return response()->json(['message' => 'ERROR']);
        }

        $file = $request->file('upload-image');
        $extension = $file->getClientOriginalExtension();
        Storage::disk('local')->put('temp/'.$file->getFilename().'.'.$extension,  File::get($file));

        return response()->json(['filename' => $file->getFilename().'.'.$extension]);
    }

    public function uploadFile(Request $request)
    {
        if (!$request->hasFile('upload-file')) {
            return response()->json(['message' => 'ERROR']);
        }

        $file = $request->file('upload-file');
        $original_filename = $file->getClientOriginalName();
        $filename = $file->getFilename() . '.' . $file->getClientOriginalExtension();

        Storage::disk('local')->put('files/'.$filename,  File::get($file));

        $item = \App\Ck\Models\File::create([
            'filename' => $filename,
            'original' => $original_filename
        ]);

        return response()->json(['id' => $item->id]);
    }

}