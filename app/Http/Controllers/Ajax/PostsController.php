<?php namespace App\Http\Controllers\Ajax;

use Auth;
use App\Ck\Helpers\AccessHelper;
use App\Ck\Helpers\TagsHelper;
use App\Ck\Models\Post;
use App\Http\Controllers\Controller;
use App\Http\Requests\AjaxPostRequest;
use Illuminate\Support\Facades\Storage;

class PostsController extends Controller {

    public function create(AjaxPostRequest $request)
    {
        $user = Auth::user();

        $data = [
            'title' => $request->input('title'),
            'lead'  => $request->input('lead'),
            'state' => 1
        ];

        if (!empty($request->input('image'))) {
            $image = $request->input('image');

            Storage::move('temp/'.$image, 'images/'.$image);
            $data['image'] = $image;
        }

        $item = Post::create($data);

        if (!empty($request->input('tags'))) {
            TagsHelper::processModelTags($item, $request->input('tags'));
        }

        $groups = AccessHelper::filterUserGroups($request->input('groups'));

        if (count($groups)) {
            $item->groups()->sync($groups);
        } else {
            $item->groups()->sync(AccessHelper::allUserGroups());
        }

        $item->branches()->sync([$user->branch_id]);

        return response()->json(['message' => url('/')]);
    }

}