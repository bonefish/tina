<?php namespace App\Http\Controllers\Ajax;

use App\Ck\Models\Tag;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Request;

class TagsController extends Controller {

    public function getList()
    {
        return response()->json(
            Tag::where('title', 'LIKE', '%' . Request::input('term') . '%')->orderBy('title')->lists('title', 'id')
        );
    }

}