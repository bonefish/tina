<?php

namespace App\Http\Controllers;

use App\Ck\Helpers\AccessHelper;
use App\Ck\Models\Post;

class StatisticController extends Controller
{
    public function __construct()
    {
        view()->share('active', 'statistic');
        view()->share('view', '');
    }

    public function index()
    {
        $items = Post::whereMustRead(1)->orderBy('title', 'ASC');

        if (!AccessHelper::isSuperAdmin()) {
            $items->whereCreatedBy(\Auth::user()->id);
        }

        $items = $items->get();

        return view('statistic.list', compact('items'));
    }

    public function show(Post $post)
    {
        if (!AccessHelper::isSuperAdmin()) {
            if (\Auth::user()->id != $post->created_by) {
                abort(403);
            }
        }

        return view('statistic.show', compact('post'));
    }
}
