<?php namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use App\Ck\Models\File as FileModel;
use Illuminate\Http\Response;

class FilesController extends Controller
{
    public function delete($id)
    {
        $file = FileModel::find($id);
        $file->delete();

        return redirect()->back();
    }

    public function download($id)
    {
        $file = FileModel::findOrFail($id);
        return response()->download(storage_path().'/app/files/' . $file->filename, $file->original);
    }

}