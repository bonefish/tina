<?php namespace App\Http\Controllers;

use App\Ck\Requests\GroupRequest;
use App\Ck\Models\Group;

class GroupsController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        view()->share('active', 'groups');
    }

    public function index()
    {
        view()->share('view', 'groups-list');

        $items = Group::whereNotIn('id', [1, 2])->orderBy('title', 'ASC')->paginate(20);
        return View('groups.index', compact('items'));
    }

    public function create()
    {
        view()->share('view', 'groups-create');

        $item = new Group();
        return View('groups.form', compact('item'));
    }

    public function store(GroupRequest $request)
    {
        Group::create($request->all());
        return redirect('groups');
    }

    public function edit($id)
    {
        view()->share('view', 'groups-edit');

        $item = Group::find($id);
        return View('groups.form', compact('item'));
    }

    public function update(GroupRequest $request, $id)
    {
        $item = Group::findOrFail($id);
        $item->update($request->all());
        return redirect('groups');
    }

}
