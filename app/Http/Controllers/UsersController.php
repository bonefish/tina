<?php

namespace App\Http\Controllers;

use Auth;
use DB;
use Hash;
use App\User;
use App\Ck\Requests\ProfileRequest;
use App\Ck\Requests\UserRequest;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class UsersController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        view()->share('active', 'users');
    }

    public function index(Request $request)
    {
        view()->share('view', 'users-list');

        $filter_groups = ['' => trans('labels.select_group')] + \App\Ck\Models\Group::orderBy('title', 'ASC')->lists('title', 'id')->toArray();
        $filter_branches = ['' => trans('labels.select_branch')] + \App\Ck\Models\Branch::orderBy('title', 'ASC')->lists('title', 'id')->toArray();

        $filters = [
            'group' => $request->input('filter_group', 0),
            'branch' => $request->input('filter_branch', 0),
            'search' => $request->input('filter_search')
        ];

        $query = User::where('active', 1);

        if ($filters['group'] > 0) {
            $query = $query->leftJoin('user_groups', 'users.id', '=', 'user_groups.user_id')->
                where('user_groups.group_id', '=', $filters['group']);
        }

        if ($filters['branch'] > 0) {
            $query = $query->leftJoin('user_branches', 'users.id', '=', 'user_branches.user_id')->
                where('user_branches.branch_id', '=', $filters['branch']);
        }

        if (!empty($filters['search'])) {
            $query = $query->where(function($q) use ($filters) {
                $q->where('users.name', 'LIKE', '%'.$filters['search'].'%')->
                    orWhere('users.email', 'LIKE', '%'.$filters['search'].'%');
            });
        }

        $items = $query->orderBy('name', 'ASC')->groupBy('users.id')->paginate(20);

        return View('users.index', compact('items', 'filter_groups', 'filter_branches', 'filters'));
    }

    public function create()
    {
        view()->share('view', 'users-create');

        $item = new User();
        return View('users.form', compact('item'));
    }

    public function store(UserRequest $request)
    {
        $data = $request->all();
        $data['password'] = Hash::make($data['password']);

        $item = User::create($data);

        if (!isset($data['branches'])) {
            $data['branches'] = array();
        }

        if (!isset($data['groups'])) {
            $data['groups'] = array();
        }

        $item->branches()->sync($data['branches']);
        $item->groups()->sync($data['groups']);

        return redirect('users');
    }

    public function edit($id)
    {
        view()->share('view', 'users-edit');

        $item = User::findOrFail($id);
        return View('users.form', compact('item'));
    }

    public function update(UserRequest $request, $id)
    {
        $item = User::findOrFail($id);
        $data = $request->all();

        if (!empty($data['password'])) {
            $data['password'] = Hash::make($data['password']);
        } else {
            unset($data['password']);
        }

        $item->update($data);

        if (!isset($data['branches'])) {
            $data['branches'] = array();
        }

        if (!isset($data['groups'])) {
            $data['groups'] = array();
        }

        $item->branches()->sync($data['branches']);
        $item->groups()->sync($data['groups']);

        return redirect('users');
    }

    public function getProfile()
    {
        view()->share('view', 'users-profile');

        $item = Auth::user();
        return View('pages.profile', compact('item'));
    }

    public function postProfile(ProfileRequest $request)
    {
        $item = Auth::user();
        $data = $request->all();

        if (!empty($data['password'])) {

            if (Hash::check($data['current_password'], $item->password)) {
                $data['password'] = Hash::make($data['password']);
            } else {
                return redirect('profile')->withErrors([0 => trans('messages.incorrect_password')]);
            }
        } else {
            unset($data['password']);
        }

        if ($request->hasFile('avatar')) {
            $file = $request->file('avatar');

            $filename = $file->getFilename();
            $extension = $file->getClientOriginalExtension();

            Storage::disk('local')->put('images/'.$filename.'.'.$extension, \File::get($file));

            $data['avatar'] = $filename . '.' . $extension;

            if (!empty($item->avatar)) {
                Storage::disk('local')->delete('images/'.$item->avatar);
            }

        } else {
            unset($data['avatar']);
        }

        $item->update($data);

        return redirect('profile');
    }

    public function removeAvatar()
    {
        $item = Auth::user();

        if (!empty($item->avatar)) {
            Storage::delete('images/'.$item->avatar);
            $item->avatar = "";
            $item->update();
        }

        return redirect('profile');
    }

    public function destroy($id)
    {
        $item = User::find($id);
        $item->active = 0;
        $item->update();
        return redirect()->back();
    }
}
