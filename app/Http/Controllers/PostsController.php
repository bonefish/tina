<?php namespace App\Http\Controllers;

use DB;
use Auth;
use App\Ck\Helpers\AccessHelper;
use App\Ck\Models\Comment;
use App\Ck\Models\Post;
use Illuminate\Http\Request;
use App\Ck\Requests\PostRequest;
use App\Ck\Requests\CommentRequest;
use Illuminate\Support\Facades\Storage;

class PostsController extends Controller
{
    protected $isAdmin;

    protected $isEditor;

    public function __construct()
    {
        parent::__construct();

        $this->middleware('auth');
        $this->middleware('auth.editor', ['only' => ['index', 'destroy']]);

        $this->isAdmin = AccessHelper::isSuperAdmin();
        $this->isEditor = AccessHelper::isAdmin();

        view()->share('active', 'posts');
    }

    public function index(Request $request)
    {
        view()->share('view', 'posts-list');

        $filter_groups = [
                '' => trans('labels.select_group')] +
            \App\Ck\Models\Group::whereNotIn('id', [1, 2])->lists('title', 'id')->toArray();
        $filter_branches = [
                '' => trans('labels.select_branch')] +
            \App\Ck\Models\Branch::lists('title', 'id')->toArray();

        $filters = [
            'group'  => $request->input('filter_group'),
            'branch' => $request->input('filter_branch'),
            'search' => $request->input('filter_search')
        ];

        $query = new Post();

        if ($filters['group']) {
            $post_ids = DB::table('group_posts')
                          ->select('post_id')
                          ->where('group_id', $filters['group'])
                          ->lists('post_id');

            $query = $query->whereIn('id', $post_ids);
        } else {
            $filters['group'] = 0;
        }

        if ($filters['branch'] > 0) {
            $post_ids = DB::table('branch_posts')
                          ->select('post_id')
                          ->where('branch_id', $filters['branch'])
                          ->lists('post_id');

            $query = $query->whereIn('id', $post_ids);
        } else {
            $filters['branch'] = 0;
        }

        if (!empty($filters['search'])) {
            $query = $query->where(function ($q) use ($filters) {
                $q->where('posts.title', 'LIKE', '%' . $filters['search'] . '%');
            });
        }

        $isAdmin = $this->isAdmin;
        $isEditor = $this->isEditor;

        if (!$this->isAdmin) {
            $query = $query->whereCreatedBy(Auth::user()->id);
        }

        $items = $query->orderBy('created_at', 'DESC')->paginate(20);

        return View(
            'posts.index',
            compact('items', 'isAdmin', 'isEditor', 'filter_groups', 'filter_branches', 'filters')
        );
    }

    public function create()
    {
        view()->share('active', '');
        view()->share('view', 'posts-create');

        $item = new Post();
        $active = '';
        $branches = [];
        $groups = [];
        $available_branches = AccessHelper::availableBranches();
        $available_groups = AccessHelper::groupsForPosting();

        if ((count($available_branches) == 0) && (count($available_groups) == 0)) {
            return redirect('/');
        }

        return View(
            'posts.form',
            compact('item', 'active', 'groups', 'branches', 'available_branches', 'available_groups')
        );
    }

    public function store(PostRequest $request)
    {
        $data = $request->all();

        $data['state'] = isset($data['state']) ? 1 : 0;
        $data['must_read'] = isset($data['must_read']) ? 1 : 0;

        $item = Post::create($data);

        if (!empty($data['image'])) {
            Storage::move('temp/' . $data['image'], 'images/' . $data['image']);
        }

        foreach ($request->input('files', []) as $file) {
            \App\Ck\Models\File::find($file)->update(['post_id' => $item->id]);
        }

        if (!isset($data['groups']) || ($data['active'] == 'branch')) {
            $data['groups'] = [];
        }

        if (!isset($data['branches']) || ($data['active'] == 'group')) {
            $data['branches'] = [];
        }

        if (!isset($data['tags'])) {
            $data['tags'] = [];
        }

        $item->groups()->sync($data['groups']);
        $item->branches()->sync($data['branches']);
        $item->tags()->sync($data['tags']);

        return redirect('posts');
    }

    public function edit($id)
    {
        $item = Post::find($id);
        $active = '';

        if (count($item->groups)) {
            $active = 'group';
        } elseif (count($item->branches)) {
            $active = 'branch';
        }

        if (AccessHelper::canEditPost($item)) {
            view()->share('view', 'posts-edit');

            $branches = [];
            $groups = [];

            foreach ($item->branches as $branch) {
                $branches[] = $branch->id;
            }

            foreach ($item->groups as $group) {
                $groups[] = $group->id;
            }

            $available_branches = AccessHelper::availableBranches();
            $available_groups = AccessHelper::groupsForPosting();

            return View(
                'posts.form',
                compact('item', 'active', 'groups', 'branches', 'available_branches', 'available_groups')
            );
        }

        return redirect('/');
    }

    public function update($id, PostRequest $request)
    {
        $data = $request->all();

        $data['state'] = isset($data['state']) ? 1 : 0;
        $data['must_read'] = isset($data['must_read']) ? 1 : 0;

        if (!isset($data['groups']) || ($data['active'] == 'branch')) {
            $data['groups'] = [];
        }

        if (!isset($data['branches']) || ($data['active'] == 'group')) {
            $data['branches'] = [];
        }

        if (!isset($data['tags'])) {
            $data['tags'] = [];
        }

        $item = Post::find($id);

        if (AccessHelper::canEditPost($item)) {
            if ($item->image != $data['image']) {
                Storage::move('temp/' . $data['image'], 'images/' . $data['image']);
                if (!empty($item->image) && Storage::has('images/' . $item->image)) {
                    Storage::delete('images/' . $item->image);
                }
            }

            $item->update($data);

            foreach ($request->input('files', []) as $file) {
                \App\Ck\Models\File::find($file)->update(['post_id' => $item->id]);
            }

            $item->groups()->sync($data['groups']);
            $item->branches()->sync($data['branches']);
            $item->tags()->sync($data['tags']);

            return redirect('posts');
        }

        return redirect('/');
    }

    public function show($id)
    {
        $item = Post::find($id);

        if (AccessHelper::canAccessPost($item)) {
            view()->share('active', '');
            view()->share('view', 'post');

            return View('posts.show', compact('item'));
        }

        return redirect('/');
    }

    public function comment(CommentRequest $request)
    {
        if (!AccessHelper::canAccessPost(Post::find($request->input('post_id')))) {
            return redirect('/');
        }

        if (!empty($request->input('comment_id'))) {
            $comment = Comment::find($request->input('comment_id'));
            $comment->comment = $request->input('comment');
            $comment->update();

            return redirect('posts/' . $comment->post_id);
        }

        $data = [
            'post_id' => $request->input('post_id'),
            'user_id' => \Auth::user()->id,
            'comment' => $request->input('comment'),
        ];

        Comment::create($data);

        return redirect('posts/' . $data['post_id']);
    }

    public function deleteComment($id)
    {
        $comment = Comment::find($id);
        $post_id = $comment->post_id;

        if ($comment->user_id == Auth::user()->id) {
            $comment->delete();
        }

        return redirect('posts/' . $post_id);
    }

    public function read($id)
    {
        $user = Auth::user();
        $item = Post::find($id);

        if (!$item->read->contains($user->id)) {
            $item->read()->attach($user->id);
        }

        return redirect('/');
    }

    public function star($id)
    {
        $user = Auth::user();
        $item = Post::find($id);

        if ($item->star->contains($user->id)) {
            $item->star()->detach($user->id);
        } else {
            $item->star()->attach($user->id);
        }

        return redirect()->back();
    }

    public function delete($id)
    {
        if (AccessHelper::isAdmin()) {
            $post = Post::find($id);
            if ($post->created_by == Auth::user()->id) {
                Post::find($id)->delete();

                return redirect('/');
            }
        }

        return redirect()->back();
    }

    public function destroy($id)
    {
        Post::find($id)->delete();

        return redirect()->back();
    }

    public function deleteImage($id)
    {
        $item = Post::find($id);

        if (!empty($item->image)) {
            if (Storage::has('images/' . $item->image)) {
                Storage::delete('images/' . $item->image);
            }
            $item->image = null;
            $item->save();
        }

        return redirect('posts/' . $item->id . '/edit');
    }

}