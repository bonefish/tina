<?php namespace App\Http\Controllers;

use App\Ck\Helpers\AccessHelper;
use App\Ck\Models\Article;
use App\Ck\Models\Post;
use Illuminate\Http\Request;
use App\Ck\Helpers\SearchHelper;

class SearchController extends Controller {

    public function search(Request $request)
    {
        $keywords = SearchHelper::prepareQuery($request->input('query'));

        $tagIds = SearchHelper::searchTags($keywords);

        $postIds = SearchHelper::searchPosts($keywords, $tagIds);
        $articleIds = SearchHelper::searchArticles($keywords, $tagIds);

        if (count($postIds)) {
            $postIds = AccessHelper::filterPostsAccess($postIds);
        }
//
//        if (count($postIds)) {
//            $postIds = AccessHelper::filterPostsAccessByBranch($postIds);
//        }


        $posts = Post::whereIn('id', $postIds)->get();
        $articles = Article::whereIn('id', $articleIds)->get();

        return View('pages.search', compact('posts', 'articles'));
    }

}