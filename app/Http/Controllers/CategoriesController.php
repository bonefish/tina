<?php namespace App\Http\Controllers;

use App\Ck\Models\Article;
use App\Ck\Models\Category;
use App\Ck\Requests\CategoryRequest;

class CategoriesController extends Controller
{

    public function __construct()
    {
        parent::__construct();
        view()->share('active', 'categories');
    }

    public function index()
    {
        view()->share('view', 'categories-list');

        $items = Category::where('parent_id', 0)->orderBy('ordering', 'ASC')->get();
        return View('categories.index', compact('items'));
    }

    public function create()
    {
        view()->share('view', 'categories-create');

        $item = new Category();
        return View('categories.form', compact('item'));
    }

    public function store(CategoryRequest $request)
    {
        Category::create($request->all());
        return redirect('categories');
    }

    public function edit($id)
    {
        view()->share('view', 'categories-edit');

        $item = Category::find($id);
        return View('categories.form', compact('item'));
    }

    public function update(CategoryRequest $request, $id)
    {
        $item = Category::find($id);
        $item->update($request->all());
        return redirect('categories');
    }

    public function show($id)
    {
        if (Article::whereState(1)->where('category_id', $id)->count() != 1) {
            view()->share('view', 'category-articles');
            view()->share('active', 'category'.$id);

            $items = Article::whereState(1)->where('category_id', $id)->orderBy('created_at', 'DESC')->paginate(20);

            return View('categories.show', compact('id', 'items'));
        } else {
            return redirect('article/'.Article::where('category_id', $id)->first()->id);
        }

    }

    public function destroy($id)
    {
        $item = Category::find($id);

        if (($item->children()->count() > 0) || ($item->articles()->count() > 0)) {
            return redirect()->back()->withErrors([0 => trans('messages.category_delete_fail')]);
        }

        $item->delete();

        return redirect()->back();
    }

    public function up($id)
    {
        $currentCategory = Category::find($id);
        if ($currentCategory->ordering > 1) {
            $tempCategory = Category::whereOrdering(($currentCategory->ordering - 1))->first();
            $tempCategory->ordering = $tempCategory->ordering + 1;
            $tempCategory->save();
            $currentCategory->ordering = $currentCategory->ordering - 1;
            $currentCategory->save();
        }
        return redirect()->back();
    }

    public function down($id)
    {
        $currentCategory = Category::find($id);
        $tempCategory = Category::whereOrdering(($currentCategory->ordering + 1))->first();
        if ($tempCategory) {
            $currentCategory->ordering = $currentCategory->ordering + 1;
            $currentCategory->save();
            $tempCategory->ordering = $tempCategory->ordering - 1;
            $tempCategory->save();
        }
        return redirect()->back();
    }

}