<?php

Route::get('login', 'Auth\AuthController@getLogin');
Route::post('login', 'Auth\AuthController@postLogin');
Route::get('logout', 'Auth\AuthController@getLogout');

Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');

Route::get('reset/sent', function () {
    return View('auth.sent');
});

Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');

Route::group(['middleware' => 'auth'], function () {
    Route::any('/profile/remove', ['as' => 'removeAvatar', 'uses' => 'UsersController@removeAvatar']);

    Route::get('/', 'HomeController@getFeed');
    Route::get('/home/{filter}/{param?}', 'HomeController@getFeed');

    Route::post('form1', ['as' => 'form1', 'uses' => 'FormsController@form1']);
    Route::post('form2', ['as' => 'form2', 'uses' => 'FormsController@form2']);

    Route::get('group/{id}', 'HomeController@getGroup');
    Route::get('category/{id}', ['as' => 'category', 'uses' => 'CategoriesController@show']);
    Route::get('ajax/tags', 'Ajax\TagsController@getList');

    Route::get('search', 'SearchController@search');

    Route::resource('posts', 'PostsController', ['except' => 'destroy']);
    Route::get('posts/read/{id}', 'PostsController@read');
    Route::get('posts/star/{id}', 'PostsController@star');
    Route::post('comment', 'PostsController@comment');
    Route::get('comments/delete/{id}', 'PostsController@deleteComment');

    Route::get('profile', 'UsersController@getProfile');
    Route::post('profile', 'UsersController@postProfile');

    Route::get('article/{id}', ['as' => 'article', 'uses' => 'ArticlesController@show']);
    Route::get('articles/deleteImage/{id}', 'ArticlesController@deleteImage');
    Route::get('posts/deleteImage/{id}', 'PostsController@deleteImage');
    Route::any('posts/deleteown/{id}', 'PostsController@delete');

    Route::get('support', function () {
        view()->share('view', 'support');
        view()->share('active', 'support');

        return View('pages.support');
    });

    Route::get('files/download/{id}', ['as' => 'downloadFile', 'uses' => 'FilesController@download']);
});

Route::group(['prefix' => 'ajax', 'middleware' => 'auth'], function () {
    Route::post('post', 'Ajax\PostsController@create');
    Route::post('uploadImage', 'Ajax\FileController@uploadImage');
    Route::post('uploadFile', 'Ajax\FileController@uploadFile');
});

Route::group(['middleware' => ['auth', 'auth.admin']], function () {
    Route::any('posts/delete/{id}', ['as' => 'deletePost', 'uses' => 'PostsController@destroy']);
    Route::any('users/delete/{id}', ['as' => 'deleteUser', 'uses' => 'UsersController@destroy']);
    Route::any('tags/delete/{id}', ['as' => 'deleteTag', 'uses' => 'TagsController@destroy']);
    Route::any('categories/delete/{id}', ['as' => 'deleteCategory', 'uses' => 'CategoriesController@destroy']);
    Route::any('categories/up/{id}', ['as' => 'upCategory', 'uses' => 'CategoriesController@up']);
    Route::any('categories/down/{id}', ['as' => 'downCategory', 'uses' => 'CategoriesController@down']);
    Route::any('articles/delete/{id}', ['as' => 'deleteArticle', 'uses' => 'ArticlesController@destroy']);

    Route::resource('articles', 'ArticlesController', ['except' => 'show']);
    Route::resource('branches', 'BranchesController', ['except' => 'show']);
    Route::resource('groups', 'GroupsController', ['except' => 'show']);
    Route::resource('tags', 'TagsController', ['except' => 'show']);
    Route::resource('users', 'UsersController', ['except' => 'show']);
    Route::resource('categories', 'CategoriesController', ['except' => 'show']);

    Route::get('files/delete/{id}', ['as' => 'deleteFile', 'uses' => 'FilesController@delete']);

    Route::get('statistic', ['as' => 'statistic', 'uses' => 'StatisticController@index']);
    Route::get('statistic/{post}', ['as' => 'statistic.show', 'uses' => 'StatisticController@show']);
});