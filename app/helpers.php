<?php

function prepareLead($text)
{
    $text = str_replace("\r\n", "<br />", $text);
    $text = addEmailLinks($text);

    return $text;
}

function prepareContent($text)
{
    $text = str_replace("&nbsp;", " ", $text);
    $text = addEmailLinks($text);

    return $text;
}

function addEmailLinks($text)
{
    $regex = "/([A-z0-9\._-]+\@[A-z0-9_-]+\.)([A-z0-9\_\-\.]{1,}[A-z])/";
    $replace = '<a href="mailto:$1$2">$1$2</a>';

    return preg_replace($regex, $replace, $text);
}

function flash($message)
{
    Session::flash('message', $message);
}

function message()
{
    return Session::get('message', null);
}