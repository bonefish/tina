<?php

return [
    'reset_subject'   => 'Resett passord',
    'reset_text'      => 'Resett passord ved å klikke på linken her:',
    'reset_signature' => 'Mvh Colosseum Klinikken',
];