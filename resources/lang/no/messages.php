<?php

return [
    'category_delete_fail' => 'Kan ikke slette kategori da den fortsatt tilhører en annen kategori og/eller har tilknyttede artikler.',
    'category_no_articles' => 'Denne kategorien har ingen artikler.',
    'comment_first'        => 'Dette innlegget har ingen kommentarer.',
    'delete_confirmation'  => 'Er du sikker på at du vil slette?',
    'form_filled'          => 'Din beskjed er innsendt. Din kontaktinfo hentes automatisk fra dine logindata og sendes sammen med din beskjed.<br /><br />Tusen takk for innspillet.',
    'form_filled2'         => 'Takk for din påmelding! Din kontaktinfo hentes automatisk fra dine logindata og legges ved. Ved ytterlig informasjon sendes dette til din e-post.',
    'list_empty'           => 'Listen er tom.',
    'upload_text'          => 'Dra et bilde hit for å laste opp, eller klikk her for å velge',
    'upload_text2'         => 'Dra en fil hit for å laste opp, eller klikk for å velge',
];
