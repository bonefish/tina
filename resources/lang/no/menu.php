<?php

return [
    'articles'   => 'Artikler',
    'branches'   => 'Klinikker',
    'categories' => 'Kategorier',
    'groups'     => 'Grupper',
    'home'       => 'Hjem',
    'logout'     => 'Logg ut',
    'posts'      => 'Innlegg',
    'profile'    => 'Profil',
    'tags'       => 'Tags',
    'statistic'  => 'Statistikk',
    'support'    => 'Rapporter feil',
    'users'      => 'Brukere'
];