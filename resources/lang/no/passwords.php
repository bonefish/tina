<?php

return [
    'reset' => 'Ditt passord har blitt resatt!',
    'sent' => 'En e-post som inkluderer en resettingslenke har blitt sendt til din innboks.',
    'token' => 'Lenke for nullstilling av passord er ikke lenger gyldig. Mottat lenke er unik, kan kun benyttes én gang og fungerer i 24 timer.',
    'user' => "Denne e-posten ligger ikke i colosseum@work sin database."
];
