<?php

return [
    'admin'                 => 'Administrasjon',
    'articles'              => 'Artikler',
    'author'                => 'Skrevet av',
    'branch'                => 'Klinikk',
    'branches'              => 'Klinikker',
    'category'              => 'Kategori',
    'change_avatar'         => 'Endre profilbilde',
    'change_name'           => 'Endre navn',
    'change_password'       => 'Endre passord',
    'choose_branches'       => 'Velg klinikk',
    'choose_groups'         => 'eller velg gruppe',
    'comments'              => 'Kommentarer',
    'count'                 => 'Lest',
    'content'               => 'Hovedtekst',
    'created'               => 'Opprettet',
    'created_by'            => 'av',
    'current_password'      => 'Gjeldende passord',
    'date'                  => 'Dato',
    'delete'                => 'Slett',
    'edit'                  => 'Endre',
    'email'                 => 'E-post',
    'files'                 => 'Vedlagte filer',
    'group'                 => 'Gruppe',
    'groups'                => 'Grupper',
    'icon'                  => 'Ikon',
    'image'                 => 'Bilde',
    'lead'                  => 'Intro tekst',
    'login'                 => 'LOGG INN',
    'mustread'              => 'Må leses',
    'name'                  => 'Navn',
    'new_password'          => 'Nytt passord',
    'new_password_confirm'  => 'Bekreft nytt passord',
    'parent_category'       => 'Hovedkategori',
    'password'              => 'Passord',
    'password_confirmation' => 'Bekreft passord',
    'post'                  => 'Publiser',
    'posts'                 => 'Innlegg',
    'published'             => 'Publisert',
    'root_category'         => 'Root',
    'search'                => 'Søk...',
    'select_branch'         => 'Velg Klinikk',
    'select_category'       => 'Velg kategori',
    'select_group'          => 'Velg Gruppe',
    'state'                 => 'Status',
    'status'                => 'Status',
    'tags'                  => 'Tags',
    'title'                 => 'Tittel',
    'unpublished'           => 'Upublisert',
    'user'                  => 'Bruker',
    'users'                 => 'Brukere',
    'written_by'            => 'Skrevet av',
];
