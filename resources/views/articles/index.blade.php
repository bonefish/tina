@extends('layouts.main')
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <div class="row">
                    <div class="col-sm-12 text-right">
                        <a href="{{ route('articles.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> {{ trans('buttons.new') }}</a>
                    </div>
                    <div class="col-sm-12 table-responsive m-t-20">
                        <table class="table table-bordered table-hover m-0">
                            <tr>
                                <th>{{ trans('labels.title') }}</th>
                                <th>{{ trans('labels.category') }}</th>
                                <th>{{ trans('labels.author') }}</th>
                                <th>{{ trans('labels.created') }}</th>
                                <th class="uk-text-center">{{ trans('labels.status') }}</th>
                                <th>{{ trans('labels.edit') }}</th>
                                <th>{{ trans('labels.delete') }}</th>
                            </tr>
                            @forelse($items as $item)
                                <tr>
                                    <td>{{ $item->title }}</td>
                                    <td>{{ @$item->category->title }}</td>
                                    <td>{{ @$item->author->name }}</td>
                                    <td>{{ $item->created_at }}</td>
                                    <td class="uk-text-center" width="1%" nowrap>
                                        <div class="label label-{{ ($item->state == 1)?'default':'pink' }}">{{ ($item->state == 1)?trans('labels.published'):trans('labels.unpublished') }}</div>
                                    </td>
                                    <td width="1%" nowrap class="text-center">
                                        <a href="{!! route('articles.edit', ['id' => $item->id]) !!}"><i class="fa fa-edit"></i></a>
                                    </td>
                                    <td width="1%" nowrap class="text-center">
                                        <a href="#" data-href="{!! route('deleteArticle', ['id' => $item->id]) !!}" data-toggle="modal" data-target="#confirm-delete"><i class="fa fa-remove"></i></a>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="7">{{ trans('messages.list_empty') }}</td>
                                </tr>
                            @endforelse
                        </table>
                    </div>
                    <div class="col-sm-12 text-center">
                        {!! $items->render() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade bs-example-modal-sm" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    {{ trans('messages.delete_confirmation') }}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('buttons.no') }}</button>
                    <a class="btn btn-danger btn-ok">{{ trans('buttons.yes') }}</a>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            $('#confirm-delete').on('show.bs.modal', function(e) {
                $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
            });
        });
    </script>
@endsection