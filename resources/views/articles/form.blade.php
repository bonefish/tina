@extends('layouts.main')
@section('content')
    <div class="row" style="max-width: 940px;">
        <div class="col-sm-12">
            <div class="card-box">
                @if ($item->id)
                    {!! Form::model($item, ['route' => ['articles.update', $item->id], 'method' => 'PUT']) !!}
                @else
                    {!! Form::model($item, ['route' => 'articles.store']) !!}
                @endif
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('category_id', trans('labels.category'), ['class' => 'control-label']) !!}
                            <select name="category_id" id="category_id" class="form-control">
                                <option>{{ trans('labels.select_category') }}</option>
                                @foreach(DataHelper::categories() as $parent_id => $parent)
                                    @if ($parent['has_children'])
                                        <option disabled="disabled">{{ $parent['title'] }}</option>
                                        @foreach ($parent['children'] as $child_id => $child)
                                            <option value="{{ $child_id }}"{{ ($item->category_id == $child_id)?' selected="selected"':'' }}>
                                                -- {{ $child['title'] }}</option>
                                        @endforeach
                                    @else
                                        <option value="{{ $parent_id }}"{{ ($item->category_id == $parent_id)?' selected="selected"':'' }}>{{ $parent['title'] }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="pull-right m-t-30">
                                {!! Form::label('state', trans('labels.published'), ['class' => 'control-label', 'style' => 'margin-right: 30px;']) !!}
                                {!! Form::checkbox('state', 1, ($item->state == 1 || empty($item->state)), ['data-plugin' => 'switchery', 'data-color' => '#81c868', 'data-switchery' => 'true']) !!}
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            {!! Form::label('lead', trans('labels.image'), ['class' => 'control-label']) !!}
                            <div class="row">
                                @if (!empty($item->image))
                                    <div class="col-md-4">
                                        {!! ImageHelper::previewImage($item->image) !!}
                                        <a href="{{ url('articles/deleteImage/'.$item->id) }}" class="btn btn-danger m-t-10"><i class="fa fa-remove"></i> {{ trans('buttons.delete_image') }}</a>
                                    </div>
                                    <div class="col-md-8">
                                        <div id="imageUploader" class="dropzone"></div>
                                        {!! Form::hidden('image') !!}
                                    </div>
                                @else
                                    <div class="col-md-12">
                                        <div id="imageUploader" class="dropzone"></div>
                                        {!! Form::hidden('image') !!}
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('title', trans('labels.title'), ['class' => 'control-label']) !!}
                            {!! Form::text('title', $item->title, ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('lead', trans('labels.lead'), ['class' => 'control-label']) !!}
                            {!! Form::textarea('lead', $item->lead, ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('content', trans('labels.content'), ['class' => 'control-label']) !!}
                            {!! Form::textarea('content', $item->content, ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('lead', trans('labels.files'), ['class' => 'control-label']) !!}
                            <div id="fileUploader" class="dropzone"></div>
                            <div id="files-list">
                                @if (count($item->files))
                                    <ul class="list-group m-t-10">
                                        @foreach($item->files as $file)
                                            <li class="list-group-item">
                                                <a href="{{ url('files/delete/' . $file->id) }}" class="badge badge-danger"><i class="fa fa-remove"></i></a>
                                                <a href="{{ url('files/download/' . $file->id) }}">{{ $file->original }}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="tags" class="control-label">{{ trans('labels.tags') }}</label>
                            <select id="tags" name="tags[]" class="select2 select2-multiple form-control" multiple="multiple">
                                @foreach(App\Ck\Models\Tag::all() as $tag)
                                    <option value="{{ $tag->id }}"{{ $item->tags->contains($tag->id)?' selected="selected"':'' }}>{{ $tag->title }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            {!! Form::hidden('id') !!}
                            {!! Form::button(trans('buttons.save'), ['type' => 'submit', 'class' => 'btn btn-primary']) !!}
                            {!! link_to_route('articles.index', trans('buttons.close'), null, ['class' => 'btn btn-danger']) !!}
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
@section('styles')
    {!! Html::style('css/form.css') !!}
    {!! Html::style('css/editor.css') !!}
    {!! Html::style('css/dropzone.css') !!}
@endsection
@section('scripts')
    {!! Html::script('js/form.js') !!}
    {!! Html::script('js/editor.js') !!}
    {!! Html::script('js/dropzone.js') !!}
    <script type="text/javascript">
        $(document).ready(function () {

            $(".select2").select2({
                width: 'resolve'
            });

            $('#content').trumbowyg();

            Dropzone.options.imageUploader = {
                paramName: 'upload-image',
                url: "{{ url('ajax/uploadImage') }}",
                maxFilesize: 10,
                maxFiles: 1,
                dictDefaultMessage: '{{ trans('messages.upload_text') }}',
                acceptedFiles: 'image/*',
                init: function () {
                    this.on('success', function (file, resp) {
                        $('[name="image"]').val(resp.filename);
                    });
                    this.on('maxfilesexceeded', function (file) {
                        this.removeAllFiles();
                        this.addFile(file);
                    });
                },
                complete: function (file) {
                    if (file.size > 10*1024*1024) {
                        alert('File is larger than 5Mb');
                        return false;
                    }
                    if (!file.type.match('image.*')) {
                        alert('Only images are allowed for this field.');
                        return false;
                    }
                }
            };

            Dropzone.options.fileUploader = {
                paramName: 'upload-file',
                url: "{{ url('ajax/uploadFile') }}",
                maxFilesize: 100,
                dictDefaultMessage: '{{ trans('messages.upload_text2') }}',
                init: function () {
                    this.on('success', function (file, resp) {
                        $('#files-list').append('<input type="checkbox" name="files[]" class="hidden" value="'+resp.id+'" checked="checked" />');
                    });
                },
                complete: function(file) {
                    if (file.size > 100*1024*1024) {
                        alert('File is larger than 100Mb');
                        return false;
                    }
                }
            };

        });
    </script>
@endsection