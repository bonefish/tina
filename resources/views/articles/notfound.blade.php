@extends('layouts.main')
@section('content')
    <div class="row" style="max-width: 934px;">
        <div class="col-md-12">
            <div class="card-box p-0">
                <article style="text-align: center; padding: 70px 50px;">
                    <h1>Beklager. Artikkelen eller innlegget du leter etter er slettet eller avpublisert.</h1>
                    <a href="{{ url('/') }}">Gå til forsiden.</a>
                </article>
            </div>
        </div>
    </div>
@endsection