@extends('layouts.main')
@section('content')
    <?php $user_id = Auth::user()->id; ?>
    <div class="row" style="max-width: 934px;">
        <div class="col-md-12">
            <div class="card-box p-0">
                <article>
                    @if(!empty($item->image))
                        {!! ImageHelper::responsiveImage($item->image) !!}
                    @endif
                    <div class="relative article-container">
                        <h1 class="article-title">{{ $item->title }}</h1>
                        <h3 class="article-lead">{!! prepareLead($item->lead) !!}</h3>
                        <div class="article-content">
                            {!! prepareContent($item->content) !!}
                        </div>
                        <a href="#" class="btn btn-back"><i class="fa fa-angle-double-left"></i> {{ trans('buttons.back') }}</a>
                    </div>
                </article>
            </div>
            @if (count($item->files))
                <h4><i class="fa fa-download"></i> {{ trans('labels.files') }}</h4>
                <div class="card-box">
                    <ul class="list-group m-b-0">
                        @foreach($item->files as $file)
                            <li class="list-group-item">
                                <a href="{{ url('files/download/' . $file->id) }}">{{ $file->original }}</a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
    </div>
@endsection