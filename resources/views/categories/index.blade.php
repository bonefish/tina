@extends('layouts.main')
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <div class="row">
                    <div class="col-sm-12 text-xs-center text-right">
                        <a href="{{ route('categories.create') }}" class="btn btn-primary"><i
                                    class="fa fa-plus"></i> {{ trans('buttons.new') }}</a>
                    </div>
                    <div class="col-sm-12 table-responsive m-t-20">
                        <table class="table table-bordered table-hover m-0">
                            <tr>
                                <th>{{ trans('labels.title') }}</th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                            @forelse($items as $i => $item)
                                <tr>
                                    <td>{{ $item->title }}</td>
                                    <td width="1%" nowrap class="text-center">
                                        @if ($i > 0)
                                            <a href="{{ url('categories/up/'.$item->id) }}"><i class="fa fa-toggle-up"></i></a>
                                        @endif
                                        @if ($i + 1 < count($items))
                                            <a href="{{ url('categories/down/'.$item->id) }}"><i class="fa fa-toggle-down"></i></a>
                                        @endif
                                    </td>
                                    <td width="1%" nowrap>
                                        <a href="{!! route('categories.edit', ['id' => $item->id]) !!}"><i
                                                    class="fa fa-edit"></i></a>
                                    </td>
                                    <td width="1%" nowrap class="text-center">
                                        <a href="#" data-href="{!! route('deleteCategory', ['id' => $item->id]) !!}"
                                           data-toggle="modal" data-target="#confirm-delete"><i
                                                    class="fa fa-remove"></i></a>
                                    </td>
                                </tr>
                                @foreach($item->children() as $child)
                                    <tr>
                                        <td>-- {{ $child->title }}</td>
                                        <td></td>
                                        <td width="1%" nowrap>
                                            <a href="{!! route('categories.edit', ['id' => $child->id]) !!}"><i
                                                        class="fa fa-edit"></i></a>
                                        </td>
                                        <td width="1%" nowrap class="text-center">
                                            <a href="#" data-href="{!! route('deleteCategory', ['id' => $child->id]) !!}"
                                               data-toggle="modal" data-target="#confirm-delete"><i
                                                        class="fa fa-remove"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            @empty
                                <tr>
                                    <td colspan="3">{{ trans('messages.list_empty') }}</td>
                                </tr>
                            @endforelse
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade bs-example-modal-sm" id="confirm-delete" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    {{ trans('messages.delete_confirmation') }}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default"
                            data-dismiss="modal">{{ trans('buttons.no') }}</button>
                    <a class="btn btn-danger btn-ok">{{ trans('buttons.yes') }}</a>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#confirm-delete').on('show.bs.modal', function (e) {
                $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
            });
        });
    </script>
@endsection