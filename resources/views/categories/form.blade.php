<?php
if ($item->id) {
    $categories = \App\Ck\Models\Category::where('parent_id', '=', 0)->where('id', '!=', $item->id)->lists('title', 'id')->all();
} else {
    $categories = \App\Ck\Models\Category::where('parent_id', '=', 0)->lists('title', 'id')->all();
}
?>
@extends('layouts.main')
@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
            @if ($item->id)
                {!! Form::model($item, ['route' => ['categories.update', $item->id], 'method' => 'PUT', 'class' => 'form-horizontal']) !!}
            @else
                {!! Form::model($item, ['route' => 'categories.store', 'class' => 'form-horizontal']) !!}
            @endif
                <div id="vue-app" class="row">
                    <div class="col-md-9">
                        <div class="form-group">
                            {!! Form::label('title', trans('labels.title'), ['class' => 'col-md-2 control-label']) !!}
                            <div class="col-md-10">
                                {!! Form::text('title', $item->title, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('parent_id', trans('labels.parent_category'), ['class' => 'col-md-2 control-label']) !!}
                            <div class="col-md-10">
                                <select v-model="selected" name="parent_id" class="form-control">
                                    <option value="0">{{ trans('labels.root_category') }}</option>
                                    @foreach ($categories as $id => $title)
                                        <option value="{{ $id }}" {{ ($id == $item->parent_id)?' selected':'' }}>{{ $title }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group" v-show="selected == 0">
                            {!! Form::label('icon', trans('labels.icon'), ['class' => 'col-md-2 control-label']) !!}
                            <div class="col-md-10">
                                <a href="#" class="btn btn-white waves-effect" v-bind:class="{'btn-default': isActive('icon-user') }" v-on:click.prevent="changeIcon('icon-user')"><i class="icon-user"></i></a>
                                <a href="#" class="btn btn-white waves-effect" v-bind:class="{'btn-default': isActive('icon-people') }" v-on:click.prevent="changeIcon('icon-people')"><i class="icon-people"></i></a>
                                <a href="#" class="btn btn-white waves-effect" v-bind:class="{'btn-default': isActive('icon-phone') }" v-on:click.prevent="changeIcon('icon-phone')"><i class="icon-phone"></i></a>
                                <a href="#" class="btn btn-white waves-effect" v-bind:class="{'btn-default': isActive('icon-map') }" v-on:click.prevent="changeIcon('icon-map')"><i class="icon-map"></i></a>
                                <a href="#" class="btn btn-white waves-effect" v-bind:class="{'btn-default': isActive('icon-location-pin') }" v-on:click.prevent="changeIcon('icon-location-pin')"><i class="icon-location-pin"></i></a>
                                <a href="#" class="btn btn-white waves-effect" v-bind:class="{'btn-default': isActive('icon-direction') }" v-on:click.prevent="changeIcon('icon-direction')"><i class="icon-direction"></i></a>
                                <a href="#" class="btn btn-white waves-effect" v-bind:class="{'btn-default': isActive('icon-directions') }" v-on:click.prevent="changeIcon('icon-directions')"><i class="icon-directions"></i></a>
                                <a href="#" class="btn btn-white waves-effect" v-bind:class="{'btn-default': isActive('icon-compass') }" v-on:click.prevent="changeIcon('icon-compass')"><i class="icon-compass"></i></a>
                                <a href="#" class="btn btn-white waves-effect" v-bind:class="{'btn-default': isActive('icon-layers') }" v-on:click.prevent="changeIcon('icon-layers')"><i class="icon-layers"></i></a>
                                <a href="#" class="btn btn-white waves-effect" v-bind:class="{'btn-default': isActive('icon-list') }" v-on:click.prevent="changeIcon('icon-list')"><i class="icon-list"></i></a>
                                <a href="#" class="btn btn-white waves-effect" v-bind:class="{'btn-default': isActive('icon-clock') }" v-on:click.prevent="changeIcon('icon-people')"><i class="icon-clock"></i></a>
                                <a href="#" class="btn btn-white waves-effect" v-bind:class="{'btn-default': isActive('icon-notebook') }" v-on:click.prevent="changeIcon('icon-notebook')"><i class="icon-notebook"></i></a>
                                <a href="#" class="btn btn-white waves-effect" v-bind:class="{'btn-default': isActive('icon-energy') }" v-on:click.prevent="changeIcon('icon-energy')"><i class="icon-energy"></i></a>
                                <a href="#" class="btn btn-white waves-effect" v-bind:class="{'btn-default': isActive('icon-speedometer') }" v-on:click.prevent="changeIcon('icon-speedometer')"><i class="icon-speedometer"></i></a>
                                <a href="#" class="btn btn-white waves-effect" v-bind:class="{'btn-default': isActive('icon-hourglass') }" v-on:click.prevent="changeIcon('icon-hourglass')"><i class="icon-hourglass"></i></a>
                                <a href="#" class="btn btn-white waves-effect" v-bind:class="{'btn-default': isActive('icon-wallet') }" v-on:click.prevent="changeIcon('icon-wallet')"><i class="icon-wallet"></i></a>
                                <a href="#" class="btn btn-white waves-effect" v-bind:class="{'btn-default': isActive('icon-speech') }" v-on:click.prevent="changeIcon('icon-speech')"><i class="icon-speech"></i></a>
                                <a href="#" class="btn btn-white waves-effect" v-bind:class="{'btn-default': isActive('icon-puzzle') }" v-on:click.prevent="changeIcon('icon-puzzle')"><i class="icon-puzzle"></i></a>
                                <a href="#" class="btn btn-white waves-effect" v-bind:class="{'btn-default': isActive('icon-pin') }" v-on:click.prevent="changeIcon('icon-pin')"><i class="icon-pin"></i></a>
                                <a href="#" class="btn btn-white waves-effect" v-bind:class="{'btn-default': isActive('icon-handbag') }" v-on:click.prevent="changeIcon('icon-handbag')"><i class="icon-handbag"></i></a>
                                <a href="#" class="btn btn-white waves-effect" v-bind:class="{'btn-default': isActive('icon-graduation') }" v-on:click.prevent="changeIcon('icon-graduation')"><i class="icon-graduation"></i></a>
                                <a href="#" class="btn btn-white waves-effect" v-bind:class="{'btn-default': isActive('icon-badge') }" v-on:click.prevent="changeIcon('icon-badge')"><i class="icon-badge"></i></a>
                                {!! Form::hidden('icon', $item->icon, ['v-model' => 'icon']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-10 col-md-offset-2">
                                {!! Form::button(trans('buttons.save'), ['type' => 'submit', 'class' => 'btn btn-primary']) !!}
                                {!! link_to_route('categories.index', trans('buttons.close'), null, ['class' => 'btn btn-danger']) !!}
                            </div>
                        </div>
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection
@section('scripts')
    {!! Html::script('js/vue.js') !!}
    <script type="text/javascript">
        new Vue({
            el: '#vue-app',
            data: {
                selected: 0,
                icon: ''
            },
            methods: {
                "changeIcon": function(id) {
                    this.icon = id;
                },
                "isActive": function(id) {
                    return (this.icon == id);
                }
            }
        });
    </script>
@endsection