@extends('layouts.main')
@section('content')
    <?php $user_id = Auth::user()->id; ?>
    <div class="row" style="max-width: 620px;">
        @forelse($items as $item)
            <div class="card-box p-0">
                {!! ImageHelper::feedImage($item->image) !!}
                <div class="text-center relative post-teaser">
                    <h2 class="post-title">{{ $item->title }}</h2>
                    <h4 class="post-lead">{!! str_replace("\r\n", "<br />", $item->lead) !!}</h4>
                    <a href="{{ url('article/'.$item->id) }}"
                       class="btn btn-primary">{{ trans('buttons.readmore') }}</a>
                </div>
            </div>
        @empty
            @if ($id == 12)
                @include('forms.form1')
            @elseif ($id == 15)
                @include('forms.form2')
            @else
                <div class="card-box">
                    <p class="m-b-0">{{ trans('messages.category_no_articles') }}</p>
                </div>
            @endif
        @endforelse
    </div>
@endsection