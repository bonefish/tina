{{ trans('emails.reset_text') }} {{ url('password/reset/'.$token) }}
<br />
<br />
{{ trans('emails.reset_signature') }}
