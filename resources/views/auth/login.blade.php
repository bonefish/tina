<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="{{ url('images/favicon.ico') }}">
    <link rel="apple-touch-icon" href="{{ url('images/favicon.png') }}"/>
    <link rel="apple-touch-icon" sizes="72x72" href="{{ url('images/favicon72x72.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ url('images/favicon114x114.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ url('images/favicon144x144.png') }}">
    <title>Colosseum</title>
    {!! Html::style('css/common.css') !!}
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <script src="js/modernizr.min.js"></script>
</head>
<body>
    <div class="animationload">
        <div class="loader"></div>
    </div>
    <div class="wrapper-page">
        <div class=" card-box">
            <div class="panel-body">
                <img src="{{ url('images/logo_blue.svg') }}" width="200px" style="margin: 5px auto 20px; display: block;" />
                @include('blocks.errors')
                {!! Form::open(['url' => 'login', 'class' => 'form-horizontal m-t-20']) !!}
                    <div class="form-group">
                        <div class="col-xs-12">
                            {!! Form::label('email', trans('labels.login')) !!}
                            {!! Form::text('email', old('email'), ['placeholder' => trans('labels.email'), 'required', 'class' => 'form-control']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            {!! Form::password('password', ['class' => 'form-control', 'required', 'placeholder' => trans('labels.password')]) !!}
                        </div>
                    </div>
                    <div class="form-group text-center m-t-20">
                        <div class="col-xs-12">
                            {!! Form::button(trans('buttons.login'), ['class' => 'btn btn-primary btn-block text-uppercase waves-effect waves-light', 'type' => 'submit']) !!}
                        </div>
                    </div>
                    <div class="form-group m-t-30 m-b-0">
                        <div class="col-sm-12">
                            <a href="{{ url('password/email') }}" class="text-dark"><i class="fa fa-lock m-r-5"></i> {{ trans('buttons.remind') }}</a>
                        </div>
                    </div>
                    {!! Form::hidden('remember', 1) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <script>
        var resizefunc = [];
    </script>
    {!! Html::script('js/common.js') !!}
</body>
</html>