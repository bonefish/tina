<div class="topbar">
    <div class="topbar-left">
        <div style="padding-left: 20px;">
            <a href="{{ url('/') }}" class="logo logo-large"><img src="{{ url('images/logo.svg') }}"
                                                                  width="140px"/></a>
            <a href="{{ url('/') }}" class="logo logo-small"><img src="{{ url('images/logo_small.svg') }}"
                                                                  width="30px"/></a>
        </div>
    </div>
    <div class="navbar navbar-default" role="navigation">
        <div class="container">
            <div class="">
                {!! Form::open(['url' => 'search', 'method' => 'GET', 'class' => 'navbar-left app-search pull-left hidden-xs']) !!}
                {!! Form::text('query', Request::input('query', null), ['placeholder' => trans('labels.search'), 'class' => 'form-control']) !!}
                <a href=""><i class="fa fa-search"></i></a>
                {!! Form::close() !!}
                <ul class="nav navbar-nav navbar-right pull-right" style="min-width: 210px;">
                    <li class="pull-right dropdown hidden-xs">
                        <a href="" class="dropdown-toggle profile" data-toggle="dropdown"
                           aria-expanded="true">{!! ImageHelper::userAvatarSmall(\Auth::user()->avatar) !!}</a>
                        <ul class="dropdown-menu">
                            <li><a href="{{ url('profile') }}"><i class="ti-user m-r-5"></i> {{ trans('menu.profile') }}
                                </a></li>
                            <li><a href="{{ url('logout') }}"><i
                                            class="ti-power-off m-r-5"></i> {{ trans('menu.logout') }}</a></li>
                        </ul>
                    </li>
                    <li class="pull-right visible-xs">
                        <button class="btn btn-post" style="margin-top: 13px; margin-left: 13px;"
                                data-toggle="offcanvas"><i class="ti-menu"></i></button>
                    </li>
                    @if(Request::is('/') || Request::is('home/*'))
                        @if (AccessHelper::canPost())
                            <li class="pull-right">
                                <div style="margin-top: 13px;">
                                    <a href="{{ route('posts.create') }}" class="btn btn-post"><span
                                                class="btn-label"><i
                                                    class="fa fa-plus"></i></span>{{ trans('buttons.new_post') }}
                                    </a>
                                    <div class="clearfix"></div>
                                </div>
                            </li>
                        @endif
                    @endif
                </ul>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>