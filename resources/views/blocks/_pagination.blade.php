@if ($paginator->lastPage() > 1)
    <ul class="uk-pagination">
        @if($paginator->currentPage() == 1)
            <li class="uk-disabled"><span><i class="uk-icon-angle-double-left"></i></span></li>
        @else
            <li><a href="{{ $paginator->url(1) }}"><i class="uk-icon-angle-double-left"></i></a></li>
        @endif
        @for ($i = 1; $i <= $paginator->lastPage(); $i++)
            @if ($paginator->currentPage() == $i)
                <li class="uk-active"><span>{{ $i }}</span></li>
            @else
                <li><a href="{{ $paginator->url($i) }}">{{ $i }}</a></li>
            @endif
        @endfor
        @if ($paginator->currentPage() == $paginator->lastPage())
            <li class="uk-disabled"><span><i class="uk-icon-angle-double-right"></i></span></li>
        @else
            <li>
                <a href="{{ $paginator->url($paginator->currentPage()+1) }}"><i class="uk-icon-angle-double-right"></i></a>
            </li>
        @endif
    </ul>
@endif