
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <p>{!! implode('<br />', $errors->all()) !!}</p>
    </div>
@endif