<div id="sidebar-menu">
    <ul>
        <li>
            <a href="{{ url('/') }}"{!! ($active == '/')?' class="active"':'' !!}><i class="ti-home"></i><span>{{ trans('menu.home') }}</span></a>
        </li>
        @foreach(DataHelper::categories() as $parent_id => $parent)
            @if ($parent['has_children'])
                <li class="has_sub">
                    <a href="#">{!! !empty($parent['icon'])?'<i class="'.$parent['icon'].'"></i>':'' !!}<span>{{ $parent['title'] }}</span> </a>
                    <ul class="list-unstyled">
                        @foreach($parent['children'] as $child_id => $child)
                            <li><a href="{{ route('category', ['id' => $child_id]) }}"{!! ($active == 'category'.$child_id)?' class="active"':'' !!}>{{ $child['title'] }}</a></li>
                        @endforeach
                    </ul>
                </li>
            @else
                <li><a href="{{ route('category', ['id' => $parent_id]) }}"{!! ($active == 'category'.$parent_id)?' class="active"':'' !!}>{!! !empty($parent['icon'])?'<i class="'.$parent['icon'].'"></i>':'' !!}<span>{{ $parent['title'] }}</span></a></li>
            @endif
        @endforeach
        @if(AccessHelper::isAdmin())
            <li class="text-muted menu-title">{{ trans('labels.admin') }}</li>
            @if(AccessHelper::isSuperAdmin())
                <li><a href="{{ url('articles') }}"{!! ($active == 'articles')?' class="active"':'' !!}><i class="icon-docs"></i><span>{{ trans('menu.articles') }}</span></a></li>
            @endif
            <li><a href="{{ url('posts') }}"{!! ($active == 'posts')?' class="active"':'' !!}><i class="icon-speech"></i><span>{{ trans('menu.posts') }}</span></a></li>
            @if(AccessHelper::isSuperAdmin())
                <li><a href="{{ url('categories') }}"{!! ($active == 'categories')?' class="active"':'' !!}><i class="icon-book-open"></i><span>{{ trans('menu.categories') }}</span></a><li>
                <li><a href="{{ route('tags.index') }}"{!! ($active == 'tags')?' class="active"':'' !!}><i class="icon-tag"></i><span>{{ trans('menu.tags') }}</span></a><li>
                <li><a href="{{ url('groups') }}"{!! ($active == 'groups')?' class="active"':'' !!}><i class="icon-people"></i><span>{{ trans('menu.groups') }}</span></a><li>
                <li><a href="{{ url('branches') }}"{!! ($active == 'branches')?' class="active"':'' !!}><i class="icon-grid"></i><span>{{ trans('menu.branches') }}</span></a><li>
                <li><a href="{{ url('users') }}"{!! ($active == 'users')?' class="active"':'' !!}><i class="icon-user"></i><span>{{ trans('menu.users') }}</span></a><li>
                <li><a href="{{ url('statistic') }}"{!! ($active == 'statistic')?' class="active"':'' !!}><i class="icon-chart"></i><span>{{ trans('menu.statistic') }}</span></a><li>
            @endif
            <li><a href="{{ url('support') }}"{!! ($active == 'support')?' class="active"':'' !!}><i class="icon-support"></i><span>{{ trans('menu.support') }}</span></a><li>
        @endif
        <li class="visible-xs"><a href="{{ url('logout') }}"><i class="ti-power-off m-r-5"></i><span>{{ trans('menu.logout') }}</span></a><li>
    </ul>
</div>
<div class="clearfix"></div>