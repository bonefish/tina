<div id="filter-block" class="uk-margin-bottom">
    {!! Form::open() !!}
        <a href="{{ url('/') }}" class="uk-button">{{ trans('buttons.all') }}</a>
        <a href="{{ url('/') }}" class="uk-button uk-button-primary">{{ trans('buttons.public') }}</a>
        <a href="{{ url('/') }}" class="uk-button uk-button-success">{{ trans('buttons.branch') }}</a>
        <a href="{{ url('/') }}" class="uk-button uk-button-danger">{{ trans('buttons.group') }}</a>
    {!! Form::close() !!}
</div>