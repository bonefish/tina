
@if (!is_null(message()))
    <div class="alert alert-success">
        <p>{!! message() !!}</p>
    </div>
@endif