<div class="row" style="max-width: 940px;">
    <div class="col-sm-12">
        <div class="card-box">
            @include('blocks.message')
            {!! Form::open(['route' => 'form2']) !!}
            <div class="form-group">
                {!! Form::label('kurs', "Navn på kurs", ['class' => 'control-label']) !!}
                <select name="kurs" class="form-control">
                    <option value="">Velg kurs</option>
                    @foreach(App\Ck\Models\Article::whereCategoryId(14)->get() as $article)
                        <option value="{{ $article->id }}">{{ $article->title }}</option>
                    @endforeach
                    </select>
                </select>
            </div>
            <div class="form-group">
                {!! Form::label('phone', "Telefonnummer", ['class' => 'control-label']) !!}
                {!! Form::text('phone', null, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('alergies', "Eventuelle allergier", ['class' => 'control-label']) !!}
                {!! Form::text('alergies', null, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::button(trans('buttons.send'), ['type' => 'submit', 'class' => 'btn btn-primary']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>