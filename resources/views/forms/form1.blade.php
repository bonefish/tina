<div class="row" style="max-width: 940px;">
    <div class="col-sm-12">
        <div class="card-box">
            @include('blocks.message')
            {!! Form::open(['route' => 'form1']) !!}
                <div class="form-group">
                    {!! Form::label('title', "Emne", ['class' => 'control-label']) !!}
                    {!! Form::text('title', null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('text', "Beskjed", ['class' => 'control-label']) !!}
                    {!! Form::textarea('text', null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::button(trans('buttons.send'), ['type' => 'submit', 'class' => 'btn btn-primary']) !!}
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>