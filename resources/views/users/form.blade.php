@extends('layouts.main', ['page_title' => trans('titles.users_page')])
@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
            @if ($item->id)
                {!! Form::model($item, ['route' => ['users.update', $item->id], 'method' => 'PUT', 'class' => 'form-horizontal']) !!}
            @else
                {!! Form::model($item, ['route' => 'users.store', 'class' => 'form-horizontal']) !!}
            @endif
            <div class="row">
                <div class="col-md-9">
                    <div class="form-group">
                        {!! Form::label('name', trans('labels.name'), ['class' => 'col-md-2 control-label']) !!}
                        <div class="col-md-10">
                            {!! Form::text('name', $item->name, ['class' => 'form-control']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('title', trans('labels.title'), ['class' => 'col-md-2 control-label']) !!}
                        <div class="col-md-10">
                            {!! Form::text('title', $item->title, ['class' => 'form-control']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('email', trans('labels.email'), ['class' => 'col-md-2 control-label']) !!}
                        <div class="col-md-10">
                            {!! Form::text('email', $item->email, ['class' => 'form-control']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('password', trans('labels.password'), ['class' => 'col-md-2 control-label']) !!}
                        <div class="col-md-10">
                            {!! Form::password('password', ['class' => 'form-control']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('password_confirmation', trans('labels.password_confirmation'), ['class' => 'col-md-2 control-label']) !!}
                        <div class="col-md-10">
                            {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('branch_id', trans('labels.branches'), ['class' => 'col-md-2 control-label']) !!}
                        <div class="col-md-10">
                            <select id="branches" name="branches[]" class="select2 select2-multiple form-control" multiple="multiple">
                                @foreach(App\Ck\Models\Branch::all() as $branch)
                                    <option value="{{ $branch->id }}"{{ $item->branches->contains($branch->id)?' selected="selected"':'' }}>{{ $branch->title }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('groups', trans('labels.groups'), ['class' => 'col-md-2 control-label']) !!}
                        <div class="col-md-10">
                            <select id="groups" name="groups[]" class="select2 select2-multiple form-control" multiple="multiple">
                                @foreach(App\Ck\Models\Group::all() as $group)
                                    <option value="{{ $group->id }}"{{ $item->groups->contains($group->id)?' selected="selected"':'' }}>{{ $group->title }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-10 col-md-offset-2">
                            {!! Form::hidden('id') !!}
                            {!! Form::button(trans('buttons.save'), ['type' => 'submit', 'class' => 'btn btn-primary']) !!}
                            {!! link_to_route('users.index', trans('buttons.close'), null, ['class' => 'btn btn-danger']) !!}
                        </div>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection
@section('styles')
    {!! Html::style('css/form.css') !!}
@endsection
@section('scripts')
    {!! Html::script('js/form.js') !!}
    <script>
        $(document).ready(function(){
            $(".select2").select2({
                width: 'resolve'
            });
        });
    </script>
@endsection