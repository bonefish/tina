@extends('layouts.main')
@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
            <div class="row">
                {!! Form::open(['method' => 'GET']) !!}
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            {!! Form::text('filter_search', $filters['search'], ['class' => 'form-control', 'placeholder' => trans('labels.search')]) !!}
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            {!! Form::select('filter_group', $filter_groups, $filters['group'], ['class' => 'form-control']) !!}
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            {!! Form::select('filter_branch', $filter_branches, $filters['branch'], ['class' => 'form-control']) !!}
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            {!! Form::button(trans('buttons.filter'), ['class' => 'btn btn-primary', 'type' => 'submit']) !!}
                            <a href="{{ route('users.create') }}" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> {{ trans('buttons.new') }}</a>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
                <div class="col-sm-12 table-responsive m-t-20">
                    <table class="table table-bordered table-hover m-0">
                        <tr>
                            <th>{{ trans('labels.name') }}</th>
                            <th>{{ trans('labels.email') }}</th>
                            <th>{{ trans('labels.branches') }}</th>
                            <th>{{ trans('labels.groups') }}</th>
                            <th></th>
                            <th>{{ trans('labels.edit') }}</th>
                            <th>{{ trans('labels.delete') }}</th>
                        </tr>
                        @forelse($items as $item)
                            <tr>
                                <td>{{ $item->name }}</td>
                                <td>{{ $item->email }}</td>
                                <td>
                                    @foreach ($item->branches as $branch)
                                        <div class="label label-info">{{ $branch->title }}</div>
                                    @endforeach
                                </td>
                                <td>
                                    @foreach ($item->groups as $group)
                                        @if (!in_array($group->id, [1, 2]))
                                            <div class="label label-pink">{{ $group->title }}</div>
                                        @endif
                                    @endforeach
                                </td>
                                <td width="1%" nowrap>
                                    @foreach ($item->groups as $group)
                                        @if (in_array($group->id, [1, 2]))
                                            <div class="label label-primary">{{ $group->title }}</div>
                                        @endif
                                    @endforeach
                                </td>
                                <td width="1%" nowrap class="text-center">
                                    <a href="{!! route('users.edit', ['id' => $item->id]) !!}"><i class="fa fa-edit"></i></a>
                                </td>
                                <td width="1%" nowrap class="text-center">
                                    <a href="#" data-href="{!! route('deleteUser', ['id' => $item->id]) !!}" data-toggle="modal" data-target="#confirm-delete"><i class="fa fa-remove"></i></a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6">{{ trans('messages.list_empty') }}</td>
                            </tr>
                        @endforelse
                    </table>
                </div>
                <div class="col-sm-12 text-center">
                    {!! $items->appends([
                        'filter_search' => $filters['search'],
                        'filter_group'  => $filters['group'],
                        'filter_branch' => $filters['branch']
                    ])->render() !!}
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade bs-example-modal-sm" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                {{ trans('messages.delete_confirmation') }}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('buttons.no') }}</button>
                <a class="btn btn-danger btn-ok">{{ trans('buttons.yes') }}</a>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            $('#confirm-delete').on('show.bs.modal', function(e) {
                $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
            });
        });
    </script>
@endsection