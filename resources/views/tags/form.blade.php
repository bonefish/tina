@extends('layouts.main')
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                @if ($item->id)
                    {!! Form::model($item, ['route' => ['tags.update', $item->id], 'method' => 'PUT', 'class' => 'form-horizontal']) !!}
                @else
                    {!! Form::model($item, ['route' => 'tags.store', 'class' => 'form-horizontal']) !!}
                @endif
                <div class="row">
                    <div class="col-md-9">
                        <div class="form-group">
                            {!! Form::label('title', trans('labels.title'), ['class' => 'col-md-2 control-label']) !!}
                            <div class="col-md-10">
                                {!! Form::text('title', $item->title, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-10 col-md-offset-2">
                                {!! Form::button(trans('buttons.save'), ['type' => 'submit', 'class' => 'btn btn-primary']) !!}
                                {!! link_to_route('tags.index', trans('buttons.close'), null, ['class' => 'btn btn-danger']) !!}
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection