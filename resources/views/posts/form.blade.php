@extends('layouts.main')
@section('content')
    <?php
    if ((count($available_groups) > 0) && (count($available_branches) == 0)) {
        $active = 'group';
    }
    if ((count($available_groups) == 0) && (count($available_branches) > 0)) {
        $active = 'branch';
    }
    ?>
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                @if ($item->id)
                    {!! Form::model($item, ['route' => ['posts.update', $item->id], 'method' => 'PUT', 'id' => 'form']) !!}
                @else
                    {!! Form::model($item, ['route' => 'posts.store', 'id' => 'form']) !!}
                @endif
                <div class="row">
                    @if (count($available_groups) && ($available_branches))
                        <div class="col-sm-12 m-b-20">
                            <button type="button" id="branch-button" class="btn btn-lg"
                                    v-on:click="toggleActive" v-bind:class="{ 'btn-info': isBranch, 'btn-white': !isBranch }">{{ trans('labels.branch') }}</button>
                            <button type="button" id="group-button" class="btn btn-lg"
                                        v-on:click="toggleActive" v-bind:class="{ 'btn-pink': isGroup, 'btn-white': !isGroup }">{{ trans('labels.group') }}</button>
                        </div>
                    @endif
                    <div class="col-md-8">
                        <div class="row">
                            @if (count($available_branches) > 1)
                                <div v-show="active=='branch'">
                                    <div class="form-group">
                                        {!! Form::label('branch_id', trans('labels.select_branch'), ['class' => 'control-label']) !!}
                                        {!! Form::select('branches[]', $available_branches, $branches,
                                            ['id' => 'branch_id', 'class' => 'form-control select2 select2-multiple', 'multiple' => 'multiple']) !!}
                                    </div>
                                </div>
                            @elseif (count($available_branches) == 1)
                                {!! Form::hidden('branches[]', array_keys($available_branches)[0]) !!}
                            @endif
                            @if (count($available_groups) > 1)
                                <div v-show="active=='group'">
                                    <div class="form-group">
                                        {!! Form::label('group_id', trans('labels.select_group'), ['class' => 'control-label']) !!}
                                        {!! Form::select('groups[]', $available_groups, $groups,
                                            ['id' => 'group_id', 'class' => 'form-control select2 select2-multiple', 'multiple' => 'multiple']) !!}
                                    </div>
                                </div>
                            @elseif (count($available_groups) == 1)
                                {!! Form::hidden('groups[]', array_keys($available_groups)[0]) !!}
                            @endif
                        </div>
                        <div v-show="active != ''">
                            <div class="form-group">
                                {!! Form::label('title', trans('labels.title'), ['class' => 'control-label']) !!}
                                {!! Form::text('title', $item->title, ['class' => 'form-control']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('lead', trans('labels.lead'), ['class' => 'control-label']) !!}
                                {!! Form::textarea('lead', $item->lead, ['class' => 'form-control']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('lead', trans('labels.image'), ['class' => 'control-label']) !!}
                                <div class="row">
                                    @if (!empty($item->image))
                                        <div class="col-md-4">
                                            {!! ImageHelper::previewImage($item->image) !!}
                                            <a href="{{ url('posts/deleteImage/'.$item->id) }}" class="btn btn-danger m-t-10"><i class="fa fa-remove"></i> {{ trans('buttons.delete_image') }}</a>
                                        </div>
                                        <div class="col-md-8">
                                            <div id="imageUploader" class="dropzone"></div>
                                            {!! Form::hidden('image') !!}
                                        </div>
                                    @else
                                        <div class="col-md-12">
                                            <div id="imageUploader" class="dropzone"></div>
                                            {!! Form::hidden('image') !!}
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                {!! Form::label('content', trans('labels.content'), ['class' => 'control-label']) !!}
                                {!! Form::textarea('content', $item->content, ['class' => 'form-control']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('lead', trans('labels.files'), ['class' => 'control-label']) !!}
                                <div id="fileUploader" class="dropzone"></div>
                                <div id="files-list">
                                    @if (count($item->files))
                                        <ul class="list-group m-t-10">
                                            @foreach($item->files as $file)
                                                <li class="list-group-item">
                                                    <a href="{{ url('files/delete/' . $file->id) }}"
                                                       class="badge badge-danger"><i class="fa fa-remove"></i></a>
                                                    <a href="{{ url('files/download/' . $file->id) }}">{{ $file->original }}</a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div v-show="active != ''">
                            <div class="p-t-0 p-20">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            {!! Form::label('state', trans('labels.published'), ['class' => 'control-label', 'style' => 'margin-right: 30px;']) !!}
                                            {!! Form::checkbox('state', 1, (($item->state == 1) || empty($item->state)), ['data-plugin' => 'switchery', 'data-color' => '#81c868', 'data-switchery' => 'true']) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            {!! Form::label('must_read', trans('labels.mustread'), ['class' => 'control-label', 'style' => 'margin-right: 30px;']) !!}
                                            {!! Form::checkbox('must_read', 1, ($item->must_read == 1), ['data-plugin' => 'switchery', 'data-color' => '#81c868', 'data-switchery' => 'true']) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="tags" class="control-label">{{ trans('labels.tags') }}</label>
                                    <select id="tags" name="tags[]" class="select2 select2-multiple form-control"
                                            multiple="multiple">
                                        @foreach(App\Ck\Models\Tag::all() as $tag)
                                            <option value="{{ $tag->id }}"{{ $item->tags->contains($tag->id)?' selected="selected"':'' }}>{{ $tag->title }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group" v-show="active!=''">
                        {!! Form::hidden('id') !!}
                        {!! Form::button(trans('buttons.save'), ['type' => 'submit', 'class' => 'btn btn-primary']) !!}
                        {!! link_to_route('posts.index', trans('buttons.close'), null, ['class' => 'btn btn-danger']) !!}
                    </div>
                </div>
                {!! Form::hidden('active', $active, ['v-model="active"']) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
@section('styles')
    {!! Html::style('css/form.css') !!}
    {!! Html::style('css/editor.css') !!}
    {!! Html::style('css/dropzone.css') !!}
@endsection
@section('scripts')
    {!! Html::script('js/vue.js') !!}
    {!! Html::script('js/form.js') !!}
    {!! Html::script('js/editor.js') !!}
    {!! Html::script('js/dropzone.js') !!}
    <script type="text/javascript">
        $(document).ready(function () {

            $(".select2").select2({
                width: '100%'
            });

            $('#content').trumbowyg();

            Dropzone.options.imageUploader = {
                paramName: 'upload-image',
                url: "{{ url('ajax/uploadImage') }}",
                maxFilesize: 10,
                maxFiles: 1,
                dictDefaultMessage: '{{ trans('messages.upload_text') }}',
                acceptedFiles: 'image/*',
                init: function () {
                    this.on('success', function (file, resp) {
                        $('[name="image"]').val(resp.filename);
                    });
                    this.on('maxfilesexceeded', function (file) {
                        this.removeAllFiles();
                        this.addFile(file);
                    });
                },
                complete: function (file) {
                    if (file.size > 10 * 1024 * 1024) {
                        alert('File is larger than 5Mb');
                        return false;
                    }
                    if (!file.type.match('image.*')) {
                        alert('Only images are allowed for this field.');
                        return false;
                    }
                }
            };

            Dropzone.options.fileUploader = {
                paramName: 'upload-file',
                url: "{{ url('ajax/uploadFile') }}",
                maxFilesize: 100,
                dictDefaultMessage: '{{ trans('messages.upload_text2') }}',
                init: function () {
                    this.on('success', function (file, resp) {
                        $('#files-list')
                                .append('<input type="checkbox" name="files[]" class="hidden" value="' + resp.id + '" checked="checked" />');
                    });
                },
                complete: function (file) {
                    if (file.size > 100 * 1024 * 1024) {
                        alert('File is larger than 100Mb');
                        return false;
                    }
                }
            };

        });

        new Vue({
            el: '#form',
            data: {
                active: '{{ $active }}',
                isGroup: {{ ($active == 'group')?'true':'false' }},
                isBranch: {{ ($active == 'branch')?'true':'false' }},
            },
            methods: {
                toggleActive: function (event) {
                    if (event.target.id == 'branch-button') {
                        this.active = 'branch';
                    } else if (event.target.id = 'group-button') {
                        this.active = 'group';
                    }
                }
            },
            watch: {
                active: function(val) {
                    if (val == 'group') {
                        this.isGroup = true;
                        this.isBranch = false;
                    }

                    if (val == 'branch') {
                        this.isGroup = false;
                        this.isBranch = true;
                    }
                }
            }
        });
    </script>
@endsection