@extends('layouts.main')
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <div class="row">
                    @if ($isAdmin)
                        {!! Form::open(['method' => 'GET', 'id' => 'filter-form']) !!}
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    {!! Form::text('filter_search', $filters['search'], ['class' => 'form-control', 'placeholder' => trans('labels.search')]) !!}
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    {!! Form::select('filter_group', $filter_groups, $filters['group'], ['v-model' => 'group', 'class' => 'form-control']) !!}
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    {!! Form::select('filter_branch', $filter_branches, $filters['branch'], ['v-model' => 'branch', 'class' => 'form-control']) !!}
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    {!! Form::button(trans('buttons.filter'), ['class' => 'btn btn-primary', 'type' => 'submit']) !!}
                                    <a href="{{ route('posts.create') }}" class="btn btn-primary pull-right"><i
                                                class="fa fa-plus"></i> {{ trans('buttons.new') }}</a>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    @else
                        <a href="{{ route('posts.create') }}" class="btn btn-primary pull-right"><i
                                    class="fa fa-plus"></i> {{ trans('buttons.new') }}</a>
                        <div class="clearfix"></div>
                    @endif
                    <div class="col-sm-12 table-responsive m-t-20">
                        <table class="table table-bordered table-hover m-0">
                            <tr>
                                <th>{{ trans('labels.title') }}</th>
                                <th>{{ trans('labels.group') }}</th>
                                <th>{{ trans('labels.author') }}</th>
                                <th>{{ trans('labels.created') }}</th>
                                <th class="uk-text-center">{{ trans('labels.status') }}</th>
                                <th>{{ trans('labels.edit') }}</th>
                                <th>{{ trans('labels.delete') }}</th>
                            </tr>
                            @forelse($items as $item)
                                <tr>
                                    <td>{{ $item->title }}</td>
                                    <td>
                                        @foreach ($item->groups as $group)
                                            <div class="label label-pink">{{ $group->title }}</div>
                                        @endforeach
                                        @foreach ($item->branches as $branch)
                                            <div class="label label-info">{{ $branch->title }}</div>
                                        @endforeach
                                    </td>
                                    <td width="1%" nowrap>{{ @$item->author->name }}</td>
                                    <td width="1%" nowrap>{{ $item->created_at }}</td>
                                    <td class="uk-text-center" width="1%" nowrap>
                                        <div class="label label-{{ ($item->state == 1)?'default':'pink' }}">{{ ($item->state == 1)?trans('labels.published'):trans('labels.unpublished') }}</div>
                                    </td>
                                    <td width="1%" nowrap class="text-center">
                                        <a href="{!! route('posts.edit', ['id' => $item->id]) !!}"><i
                                                    class="fa fa-edit"></i></a>
                                    </td>
                                    <td width="1%" nowrap class="text-center">
                                        <a href="#" data-href="{!! route('deletePost', ['id' => $item->id]) !!}"
                                           data-toggle="modal" data-target="#confirm-delete"><i
                                                    class="fa fa-remove"></i></a>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="7">{{ trans('messages.list_empty') }}</td>
                                </tr>
                            @endforelse
                        </table>
                    </div>
                    <div class="col-sm-12 text-center">
                        {!! $items->render() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade bs-example-modal-sm" id="confirm-delete" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    {{ trans('messages.delete_confirmation') }}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default"
                            data-dismiss="modal">{{ trans('buttons.no') }}</button>
                    <a class="btn btn-danger btn-ok">{{ trans('buttons.yes') }}</a>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    {!! Html::script('js/vue.js') !!}
    <script type="text/javascript">
        $(document).ready(function () {
            $('#confirm-delete').on('show.bs.modal', function (e) {
                $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
            });
        });
        @if ($isAdmin)
            new Vue({
                el: '#filter-form',
                data: {
                    group: {{ $filters['group'] }},
                    branch: {{ $filters['branch'] }}
                },
                watch: {
                    'group': function (val) {
                        if (val > 0) {
                            this.branch = 0;
                        }
                    },
                    'branch': function (val) {
                        if (val > 0) {
                            this.group = 0;
                        }
                    }
                }
            });
        @endif
    </script>
@endsection