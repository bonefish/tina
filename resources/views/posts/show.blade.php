@extends('layouts.main')
<?php
$btn_class = '';
if (count($item->branches) > 0) {
    $tmp_item = \App\Ck\Models\Post::find($item->id);
    if ($tmp_item->branches()->where('id', 1)->count()) {
        $btn_class = 'btn-primary';
    } else {
        $btn_class = 'btn-info';
    }
} else if (count($item->groups) > 0) {
    $btn_class = 'btn-pink';
}
?>
@section('content')
    <?php $user_id = Auth::user()->id; ?>
    <?php $star_active = $item->star->contains($user_id); ?>
    <div class="row" style="max-width: 934px;">
        <div class="col-md-12">
            <div class="card-box p-0">
                @if(!empty($item->image))
                    {!! ImageHelper::responsiveImage($item->image) !!}
                @endif
                <div class="relative post-content">
                    <a href="{{ url('posts/star/'.$item->id) }}"
                    class="btn-star star-relative star-smaller {{ ($star_active)?'star-active':'' }}"><i
                        class="ion-ios7-star"></i></a>
                    @if ($item->must_read && !$item->read->contains($user_id))
                        <div class="must-read btn {{ $btn_class }} btn-xs">{{ trans('labels.mustread') }}</div>
                    @endif
                    <span class="post-details">{{ trans('labels.written_by') }} {{ $item->author->name }} - {{ \Carbon\Carbon::createFromTimestamp(strtotime($item->created_at))->format('d.m.Y') }}</span>
                    <h1 class="post-title">{{ $item->title }}</h1>
                    <h3 class="post-lead">{!! prepareLead($item->lead) !!}</h3>
                    <div class="post-text">
                        {!! prepareContent($item->content) !!}
                    </div>
                    <a href="#" class="btn btn-back"><i class="fa fa-angle-double-left"></i> {{ trans('buttons.back') }}</a>
                    @if ($item->must_read && !$item->read->contains($user_id))
                        <a href="{{ url('posts/read/'.$item->id) }}"
                           class="btn btn-primary">{{ trans('buttons.read') }}</a>
                    @endif
                    @if ($item->created_by == Auth::user()->id)
                        <a href="{{ route('posts.edit', ['id' => $item->id]) }}" class="btn btn-info">{{ trans('buttons.edit') }}</a>
                        @if (AccessHelper::isAdmin())
                            <a href="{{ url('posts/deleteown/'.$item->id) }}" class="btn btn-danger">{{ trans('buttons.delete') }}</a>
                        @endif
                    @endif
                </div>
            </div>
            @if (count($item->files))
                <h4><i class="fa fa-download"></i> {{ trans('labels.files') }}</h4>
                <div class="card-box">
                    <ul class="list-group m-b-0">
                        @foreach($item->files as $file)
                            <li class="list-group-item">
                                <a href="{{ url('files/download/' . $file->id) }}">{{ $file->original }}</a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if ($item->groups()->count() > 0)
                <h4><i class="fa fa-comments-o"></i> {{ trans('labels.comments') }}</h4>
                <div class="card-box">
                    <div class="list-group m-b-0">
                        @forelse ($item->comments()->orderBy('created_at', 'ASC')->get() as $comment)
                            <div class="media">
                                <div class="media-left">
                                    {!! ImageHelper::userAvatarMedium($comment->user->avatar) !!}
                                </div>
                                <div class="media-body">
                                    <p class="comment-text">{{ $comment->comment }}</p>
                                    <p class="comment-details">{{ $comment->user->name }}
                                        , {{ Carbon::createFromTimeStamp(strtotime($comment->created_at))->diffForHumans() }}
                                        @if ($comment->user->id == $user_id)
                                            |
                                            <a href="" class="edit-comment" data-attr="{{ $comment->id }}" data-text="{{ $comment->comment }}">{{ trans('buttons.edit') }}</a>
                                            |
                                            <a href="{{ url('comments/delete/'.$comment->id) }}">{{ trans('buttons.delete') }}</a>
                                        @endif
                                    </p>

                                </div>
                            </div>
                        @empty
                            <p class="m-b-0">{{ trans('messages.comment_first') }}</p>
                        @endforelse
                    </div>
                </div>
                <div class="card-box">
                    {!! Form::open(['url' => url('comment'), 'method' => 'POST']) !!}
                    <div class="form-group">
                        {!! Form::textarea('comment', null, ['class' => 'form-control', 'style' => 'height: 75px; resize: none;']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::button(trans('buttons.comment'), ['type' => 'submit', 'class' => 'btn btn-primary']) !!}
                        {!! Form::button(trans('buttons.cancel'), ['id' => 'btn-cancel', 'class' => 'btn btn-danger hidden']) !!}
                    </div>
                    {!! Form::hidden('post_id', $item->id) !!}
                    {!! Form::hidden('comment_id', null) !!}
                    {!! Form::close() !!}
                </div>
            @endif
        </div>
    </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            $('a.edit-comment').on('click', function(ev){
                ev.preventDefault();
                $('textarea.form-control').val($(this).attr('data-text')).focus();
                $('input[name="comment_id"]').val($(this).attr('data-attr'));
                $('#btn-cancel').removeClass('hidden');
            });
            $('#btn-cancel').on('click', function(ev){
                ev.preventDefault();
                $('textarea.form-control').val('');
                $('input[name="comment_id"]').val('');
                $('#btn-cancel').addClass('hidden');
            });
        });
    </script>
@endsection