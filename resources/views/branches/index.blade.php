@extends('layouts.main')
@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
            <div class="row">
                <div class="col-sm-12 text-right">
                    <a href="{{ route('branches.create') }}" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> {{ trans('buttons.new') }}</a>
                </div>
                <div class="col-sm-12 table-responsive m-t-20">
                    <table class="table table-bordered table-hover m-0">
                        <tr>
                            <th>{{ trans('labels.title') }}</th>
                            <th>{{ trans('labels.users') }}</th>
                            <th></th>
                        </tr>
                        @forelse($items as $item)
                            <tr>
                                <td>{{ $item->title }}</td>
                                <td width="1%" nowrap>{{ count($item->users) }}</td>
                                <td width="1%" nowrap>
                                    <a href="{!! route('branches.edit', ['id' => $item->id]) !!}"><i class="fa fa-edit"></i></a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="2">{{ trans('messages.list_empty') }}</td>
                            </tr>
                        @endforelse
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection