@extends('layouts.main')
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <div class="row">
                    <div class="col-sm-12 table-responsive m-t-20">
                        <table class="table table-bordered table-hover m-0">
                            <tr>
                                <th>{{ trans('labels.title') }}</th>
                                <th></th>
                                <th>{{ trans('labels.count') }}</th>
                            </tr>
                            @foreach($items as $item)
                                <tr>
                                    <td>
                                        <a href="{{ route('statistic.show', $item->id) }}">
                                            {{ $item->title }}
                                        </a>
                                    </td>
                                    <td>
                                        @foreach ($item->branches as $branch)
                                            <div class="label label-info">{{ $branch->title }}</div>
                                        @endforeach
                                        @foreach ($item->groups as $group)
                                            @if (!in_array($group->id, [1, 2]))
                                                <div class="label label-pink">{{ $group->title }}</div>
                                            @endif
                                        @endforeach
                                    </td>
                                    <td class="text-center" width="1%" nowrap>
                                        {{ $item->read->count() }}
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection