@extends('layouts.main')
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <div class="row">
                    <div class="col-sm-12 table-responsive m-t-20">
                        <h3>{{ $post->title }}</h3>
                        <table class="table table-bordered table-hover m-0">
                            <tr>
                                <th>{{ trans('labels.name') }}</th>
                                <th>{{ trans('labels.date') }}</th>
                            </tr>
                            @foreach($post->readers() as $reader)
                                <tr>
                                    <td>{{ $reader->name}}</td>
                                    <td class="text-center" width="1%" nowrap>{{ $reader->created_at }}</td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection