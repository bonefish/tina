<b>Emne:</b> {{ $title }}<br />
<b>Beskjed:</b> {{ $text }}<br />
<b>Fullt navn:</b> {{ Auth::user()->name }}<br />
<b>Klinikk:</b> {{ implode(", ", Auth::user()->branches()->lists('title')->toArray()) }}