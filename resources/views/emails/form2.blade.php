<?php $article = App\Ck\Models\Article::find($kurs); ?>
<b>Navn på kurs:</b> {{ $article->title }}<br/>
<b>Fullt navn:</b> {{ Auth::user()->name }}<br/>
<b>Tittel:</b> {{ Auth::user()->title }}<br/>
<b>Klinikk:</b> {{ implode(", ", Auth::user()->branches()->lists('title')->toArray()) }}<br/>
<b>Email:</b> {{ Auth::user()->email }}<br/>
<b>Telefonnummer:</b> {{ $phone }}
@if (!empty($alergies))
    <br/><b>Eventuelle allergier:</b> {{ $alergies }}
@endif