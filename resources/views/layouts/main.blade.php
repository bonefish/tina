<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="{{ url('images/favicon.ico') }}">
    <link rel="apple-touch-icon" href="{{ url('images/favicon.png') }}"/>
    <link rel="apple-touch-icon" sizes="72x72" href="{{ url('images/favicon72x72.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ url('images/favicon114x114.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ url('images/favicon144x144.png') }}">
    <meta name="application-name" content="Colosseum" data-view="{{ $view }}"/>
    <title>Colosseum</title>
    {!! Html::style('css/common.css') !!}
    @yield('styles')
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    {!! Html::script('js/modernizr.min.js') !!}
</head>
<body class="fixed-left">
<div class="animationload">
    <div class="loader"></div>
</div>
<div id="wrapper">
    @include('blocks.topbar')
    <div class="left side-menu row-offcanvas row-offcanvas-right">
        <div class="sidebar-inner slimscrollleft">
            @include('blocks.nav')
        </div>
    </div>
    <div class="content-page">
        <div class="content">
            <div class="container">
                @include('blocks.errors')
                @yield('content')
            </div>
        </div>
        <footer class="footer text-right">
            © Colosseumklinikken {{ date("Y") }}
        </footer>
    </div>
</div>
<script>
    var resizefunc = [];
</script>
{!! Html::script('js/common.js') !!}
@yield('scripts')
</body>
</html>