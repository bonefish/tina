@extends('layouts.main')
@section('content')
    <div class="row">
        {!! Form::model($item, ['files' => true]) !!}
            <div class="col-md-4">
                <div class="card-box">
                    <fieldset>
                        <legend>{{ trans('labels.change_avatar') }}</legend>
                        <div class="text-center">
                            {!! ImageHelper::profileAvatar($item->avatar) !!}
                        </div>
                        <div class="m-t-10 m-b-10">
                            <input type="file" name="avatar" class="filestyle" data-input="false">
                            @if (!empty($item->avatar))
                                <a href="{{ route('removeAvatar') }}" class="btn btn-danger m-t-5"><i class="fa fa-remove"></i> {{ trans('buttons.remove_image') }}</a>
                                <div class="clearfix"></div>
                            @endif

                        </div>
                    </fieldset>
                </div>
            </div>
            <div class="col-md-8">
                <div class="card-box">
                    <fieldset>
                        <legend>{{ trans('labels.change_name') }}</legend>
                        <div class="form-group">
                            {!! Form::text('email', $item->email, ['class' => 'form-control', 'disabled' => 'disabled']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::text('name', $item->name, ['class' => 'form-control', 'placeholder' => trans('labels.name')]) !!}
                        </div>
                    </fieldset>
                    <fieldset>
                        <legend>{{ trans('labels.change_password') }}</legend>
                        <div class="form-group">
                            {!! Form::password('current_password', ['class' => 'form-control', 'placeholder' => trans('labels.current_password')]) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::password('password', ['class' => 'form-control', 'placeholder' => trans('labels.new_password')]) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => trans('labels.new_password_confirm')]) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::button(trans('buttons.update'), ['type' => 'submit', 'class' => 'btn btn-primary']) !!}
                        </div>
                    </fieldset>
                </div>
            </div>
        {!! Form::close() !!}
    </div>
@endsection
@section('scripts')
    {!! Html::script('js/form.js') !!}
    <script type="text/javascript">
        $(document).ready(function(){
            $(":file").filestyle({
                input: false
            });
        });
    </script>
@endsection