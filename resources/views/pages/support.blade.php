@extends('layouts.main')
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <script src="https://podio.com/webforms/14698219/994964.js"></script>
                <script type="text/javascript">
                    _podioWebForm.render("994964")
                </script>
                <noscript>
                    <a href="https://podio.com/webforms/14698219/994964" target="_blank">Please fill out the form</a>
                </noscript>
            </div>
        </div>
    </div>
@endsection