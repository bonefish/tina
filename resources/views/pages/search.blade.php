@extends('layouts.main')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="search-result-box m-t-40">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#posts" data-toggle="tab" aria-expanded="true">{{ trans('labels.posts') }}<span class="badge badge-info m-l-10">{{ count($posts) }}</span></a></li>
                    <li><a href="#articles" data-toggle="tab" aria-expanded="false">{{ trans('labels.articles') }}<span class="badge badge-pink m-l-10">{{ count($articles) }}</span></a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="posts">
                        <div class="row">
                            <div class="col-md-12">
                                @forelse ($posts as $item)
                                <div class="search-item">
                                    <h3 class="h5 font-600 m-b-5">{{ $item->title }}</h3>
                                    <div class="font-13 text-success m-b-5"><a href="{{ url('/posts/' . $item->id) }}">{{ url('/posts/' . $item->id) }}</a></div>
                                    <p>{{ $item->lead }}</p>
                                    @if (count($item->tags))
                                        <p>Tags:
                                            @foreach($item->tags as $tag)
                                                <span class="label label-info">{{ $tag->title }}</span>
                                            @endforeach
                                        </p>
                                    @endif
                                </div>
                                @empty
                                    <p>{{ trans('messages.no_posts') }}</p>
                                @endforelse
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="articles">
                        <div class="row">
                            <div class="col-md-12">
                                @forelse ($articles as $item)
                                    <div class="search-item">
                                        <h3 class="h5 font-600 m-b-5">{{ $item->title }}</h3>
                                        <div class="font-13 text-success m-b-5"><a href="{{ url('/articles/' . $item->id) }}">{{ url('/articles/' . $item->id) }}</a></div>
                                        <p>{{ $item->lead }}</p>
                                        @if (count($item->tags))
                                            <p>Tags:
                                                @foreach($item->tags as $tag)
                                                    <span class="label label-info">{{ $tag->title }}</span>
                                                @endforeach
                                            </p>
                                        @endif
                                    </div>
                                @empty
                                    <p>{{ trans('messages.no_articles') }}</p>
                                @endforelse
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection