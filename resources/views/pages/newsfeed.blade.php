@extends('layouts.main')
@section('content')
    <?php $user_id = Auth::user()->id; ?>
    <div class="row m-b-20" style="max-width: 620px;">
        <div class="col-md-12">
            <a href="{{ url('/') }}" class="btn btn-default">{{ trans('buttons.all') }}</a>
            <a href="{{ url('/home/main') }}" class="btn btn-primary">Colosseum</a>
            @if (count($filter_branches) == 1)
                <a href="{{ url('/home/clinic/'.$filter_branches[0]->id) }}"
                   class="btn btn-info">{{ trans('buttons.branch') }}</a>
            @elseif (count($filter_branches) > 1)
                <div class="btn-group dropdown">
                    <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                        {{ ($active['branch'])?$active['branch']:trans('buttons.branch') }} <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu">
                        @foreach ($filter_branches as $branch)
                            <li><a href="{{ url('home/clinic/'.$branch->id) }}">{{ $branch->title }}</a></li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if (count($filter_groups) == 1)
                <a href="{{ url('/home/groups') }}" class="btn btn-pink">{{ trans('buttons.group') }}</a>
            @elseif (count($filter_groups) > 1)
                <div class="btn-group dropdown">
                    <button type="button" class="btn btn-pink dropdown-toggle" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                        {{ ($active['group'])?$active['group']:trans('buttons.group') }} <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu">
                        @foreach ($filter_groups as $group)
                            <li><a href="{{ url('home/groups/'.$group->id) }}">{{ $group->title }}</a></li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if (count($tags) > 0)
                <div class="btn-group dropdown">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                        {{ ($active['tag'])?$active['tag']:trans('buttons.tags') }} <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu">
                        @foreach ($tags as $tag)
                            <li><a href="{{ url('home/tags/'.$tag->id) }}">{{ $tag->title }}</a></li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <a href="{{ url('/home/starred') }}" class="btn-star btn star-active"><i
                        class="ion-ios7-star"></i></a>
        </div>
        <div class="col-md-12">
            @foreach($items as $item)
                <?php
                if ($item->branches > 0) {
                    $tmp_item = \App\Ck\Models\Post::find($item->id);
                    if ($tmp_item->branches()->where('id', 1)->count()) {
                        $btn_class = 'btn-primary';
                    } else {
                        $btn_class = 'btn-info';
                    }
                } else if ($item->groups > 0) {
                    $btn_class = 'btn-pink';
                }
                ?>
                <div class="card-box p-0">
                    {!! ImageHelper::feedImage($item->image) !!}
                    <div class="text-center relative post-teaser">
                        <h2 class="post-title">{{ $item->title }}</h2>
                        <a href="{{ url('posts/star/'.$item->id) }}"
                           class="btn-star star-relative star-smaller {{ ($item->star > 0)?'star-active ':'' }}"><i
                                    class="ion-ios7-star"></i></a>
                        @if ($item->must_read && ($item->isread == 0))
                            <div class="must-read btn {{ $btn_class }} btn-xs">{{ trans('labels.mustread') }}</div>
                        @endif
                        <h4 class="post-lead">{!! str_replace("\r\n", "<br />", $item->lead) !!}</h4>
                        <a href="{{ url('posts/'.$item->id) }}"
                           class="btn {{ $btn_class }}">{{ trans('buttons.readmore') }}</a>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection