$(document).ready(function(){
    $('.tm-toggle-button').on('click', function(ev){
        ev.preventDefault();
        var target = $(this).attr('data-target');
        if ($('#'+target).is(':checked')) {
            $(this).removeClass('uk-active');
            $('#'+target).prop('checked', false);
        } else {
            $(this).addClass('uk-active');
            $('#'+target).prop('checked', true);
        }
    });
});