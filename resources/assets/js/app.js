$(document).ready(function () {
    $('.ajax-submit').on('click', function (ev) {
        ev.preventDefault();
        ajaxSubmitForm();
    });

    $('.btn-back').on('click', function(ev) {
        ev.preventDefault();
        parent.history.back();
    });

    $('[data-toggle="offcanvas"]').click(function (ev) {
        ev.preventDefault();
        $('.row-offcanvas').toggleClass('active')
    });

});

function ajaxSubmitForm(form) {
    var form = $('#ajax-form');
    var action = form.attr('action'),
        method = form.attr('method');

    $.ajax({
        url: action,
        method: method,
        data: form.serialize(),
        success: function (resp) {
            if (resp.message == "OK") {
                $('#post-modal').modal('hide');
            }
            else {
                window.location.replace(resp.message);
            }
        },
        error: function (response) {
            $.each(JSON.parse(response.responseText), function (i, v) {
                $.Notification.notify('error','top right', 'Feil', v);
            });
        }
    });
}