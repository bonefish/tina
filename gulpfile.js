var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function (mix) {
    mix
        .scripts([
            "./bower_components/jquery-2.2.4.min/index.js",
            "./bower_components/bootstrap/dist/js/bootstrap.js",
            "detect.js",
            "./bower_components/jquery-slimscroll/jquery.slimscroll.js",
            "jquery.blockUI.js",
            "jquery.nicescroll.js",
            "jquery.scrollTo.min.js",
            "./bower_components/switchery/dist/switchery.js",
            "jquery.core.js",
            "jquery.app.js",
            "app.js"
        ], "public/js/common.js")
        .scripts("./bower_components/vue/dist/vue.js", "public/js/vue.js")
        .scripts("modernizr.min.js", "public/js/modernizr.min.js")
        .scripts("dropzone.js", "public/js/dropzone.js")
        .scripts([
            "./bower_components/trumbowyg/dist/trumbowyg.js"
        ], "public/js/editor.js")
        .scripts([
            "./bower_components/jquery-ui/jquery-ui.js",
            "./bower_components/select2/dist/js/select2.js",
            "bootstrap-filestyle.js",
        ], "public/js/form.js")
        .styles([
            "./bower_components/bootstrap/dist/css/bootstrap.css",
            "core.css",
            "components.css",
            "icons.css",
            "pages.css",
            "responsive.css",
            "./bower_components/switchery/dist/switchery.css",
            "custom.css",
        ], "public/css/common.css")
        .styles([
            "./bower_components/select2/dist/css/select2.css",
        ], "public/css/form.css")
        .styles([
            "basic.css",
            "dropzone.css"
        ], "public/css/dropzone.css")
        .styles("./bower_components/trumbowyg/dist/ui/trumbowyg.css", "public/css/editor.css")
        .copy("./bower_components/trumbowyg/dist/ui/images", "public/css/images")
        .copy("./bower_components/font-awesome/fonts", "public/fonts");
});